package bedees.lib.superpoweredfx;

/**
 * Created by Brayo on 9/2/2016.
 */
public class SuperpoweredFX {

    public void init(int samplerate, int buffersize, String apkPath, int fileAoffset, int fileAlength, int fileBoffset, int fileBlength){
        SuperpoweredExample(samplerate, buffersize, apkPath, fileAoffset, fileAlength, fileBoffset, fileBlength);
    }

    public void playPause(boolean state){
        onPlayPause(state);
    }

    public void crossFader(int value){
        onCrossfader(value);
    }

    public void fxSelect(int value){
        onFxSelect(value);
    }

    public void fxOff(){
        onFxOff();
    }

    public void fxValue(int value){
        onFxValue(value);
    }

    static {
        System.loadLibrary("SuperpoweredExample");
    }

    private native void SuperpoweredExample(int samplerate, int buffersize, String apkPath, int fileAoffset, int fileAlength, int fileBoffset, int fileBlength);

    private native void onPlayPause(boolean play);

    private native void onCrossfader(int value);

    private native void onFxSelect(int value);

    private native void onFxOff();

    private native void onFxValue(int value);
}
