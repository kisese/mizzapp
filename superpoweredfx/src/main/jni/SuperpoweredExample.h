#ifndef Header_SuperpoweredExample
#define Header_SuperpoweredExample

#include <math.h>
#include <pthread.h>

#include "SuperpoweredExample.h"
#include <SuperpoweredAdvancedAudioPlayer.h>
#include <SuperpoweredFilter.h>
#include <SuperpoweredRoll.h>
#include <SuperpoweredFlanger.h>
#include <SuperpoweredWhoosh.h>
#include <SuperpoweredEcho.h>
#include <SuperpoweredReverb.h>
#include <SuperpoweredLimiter.h>
#include <SuperpoweredGate.h>
#include <SuperpoweredCompressor.h>
#include <Superpowered3BandEQ.h>
#include <AndroidIO/SuperpoweredAndroidAudioIO.h>

#define HEADROOM_DECIBEL 3.0f
static const float headroom = powf(10.0f, -HEADROOM_DECIBEL * 0.025f);

class SuperpoweredExample {
public:

	SuperpoweredExample(unsigned int samplerate, unsigned int buffersize, const char *path, int fileAoffset, int fileAlength, int fileBoffset, int fileBlength);
	~SuperpoweredExample();

	bool process(short int *output, unsigned int numberOfSamples);
	void onPlayPause(bool play);
	void onCrossfader(int value);
	void onFxSelect(int value);
	void onFxOff();
	void onFxValue(int value);

private:
    pthread_mutex_t mutex;
    SuperpoweredAndroidAudioIO *audioSystem;
    SuperpoweredAdvancedAudioPlayer *playerA, *playerB;
    SuperpoweredRoll *roll;
    SuperpoweredFilter *filter;
    SuperpoweredFlanger *flanger;
	SuperpoweredWhoosh *whoosh;
	SuperpoweredEcho *echo;
	SuperpoweredReverb *reverb;
	SuperpoweredLimiter *limiter;
	SuperpoweredGate *gate;
	SuperpoweredCompressor *compressor;
	Superpowered3BandEQ *bandEQ;
	float *stereoBuffer;
    unsigned char activeFx;
    float crossValue, volA, volB;
};

#endif
