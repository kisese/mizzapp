package com.bedees.android.model;

import com.bedees.android.util.TimeUtils;

/**
 * Created by MOHIT on 27-12-2015.
 */
public class Contact {
    private String contactId;
    private String name;
    private String phoneNumber;
    private String countryCode;
    private String normalizedNumber;

    public String getNormalizedNumber() {
        return normalizedNumber!=null?normalizedNumber:phoneNumber;
    }

    public void setNormalizedNumber(String normalizedNumber) {
        this.normalizedNumber = normalizedNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private String imageUrl;

    public long getLastContacted() {
        return lastContacted;
    }

    public void setLastContacted(long lastContacted) {
        this.lastContacted = lastContacted;
    }

    public String getFormattedTime(){
        String timeAgo= TimeUtils.getTimeAgo(lastContacted);
        return timeAgo;
    }

    private long lastContacted;

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
