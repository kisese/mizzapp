package com.bedees.android.listeners;

import com.bedees.android.model.Contact;

/**
 * Created by vitaliy.herasymchuk on 7/9/16.
 */
public interface OnProfilePopupListener {

    void onShowProfilePopup(Contact contact);

}
