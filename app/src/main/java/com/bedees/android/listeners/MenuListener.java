package com.bedees.android.listeners;

import android.view.View;



public interface MenuListener {
    public void onMenuClick(View view, int position);
}
