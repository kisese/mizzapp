package com.bedees.android.listeners;

import android.view.View;


public interface CommonClickListener {
    public void onCommonClick(View view, int position);
}
