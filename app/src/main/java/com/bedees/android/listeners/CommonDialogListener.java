package com.bedees.android.listeners;

import android.os.Bundle;



public interface CommonDialogListener {
    public void onPositiveClick(int mDialogType, Bundle bundle);
}
