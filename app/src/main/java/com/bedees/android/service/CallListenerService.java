package com.bedees.android.service;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bedees.android.R;
import com.bedees.android.ui.MainActivity;
import com.bedees.android.ui.ReceiveActivity;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.SavePref;
import com.bedees.android.util.Utilities;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.Timer;
import java.util.TimerTask;

public class CallListenerService extends Service {

    public static final String TAG = CallListenerService.class.getSimpleName();
    private Firebase mFirebase;
    private SavePref pref;
    private int timer_value=11;
    private boolean calling = true;
    private Timer timer;
    private Context context;




   /* public CallListenerService() {
  //      super("CallListener");

    }*/

 /*   @Override
    protected void onHandleIntent(Intent intent) {

    }
*/

    /*@Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        SavePref pref = new SavePref(getApplicationContext());
        pref.setAppStatus("off");
        Toast.makeText(getApplicationContext(), "onTaskRemoved = "+pref.getAppStatus(), Toast.LENGTH_SHORT).show();

        Intent intent= new Intent(this, CallListenerService.class);
        startService(intent);
    }
*/


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        // TODO Auto-generated method stub
        try {
            Intent restartService = new Intent(getApplicationContext(),
                    this.getClass());
            restartService.setPackage(getPackageName());
            PendingIntent restartServicePI = PendingIntent.getService(
                    getApplicationContext(), 1, restartService,
                    PendingIntent.FLAG_ONE_SHOT);
            AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


   /*  @Override
    public void onTaskRemoved(Intent rootIntent) {

         int currentapiVersion = android.os.Build.VERSION.SDK_INT;
         if (currentapiVersion <= Build.VERSION_CODES.KITKAT){

             Toast.makeText(getApplicationContext(), "onTaskRemoved", Toast.LENGTH_SHORT).show();

             Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
             restartServiceIntent.setPackage(getPackageName());

             PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
             AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
             alarmService.set(
                     AlarmManager.ELAPSED_REALTIME,
                     SystemClock.elapsedRealtime() + 1000,
                     restartServicePendingIntent);

             super.onTaskRemoved(rootIntent);

         }


    }
*/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
try {
    context = this;

    pref = new SavePref(getApplicationContext());

    //Toast.makeText(this,"onStartCommand", Toast.LENGTH_SHORT).show();


    final String normalizedPhone = Utilities.getVerifiedNormalizedPhoneNumber(this);
    Log.e(TAG, "onStartCommand");
    mFirebase = new Firebase(FirebaseConstants.FIRE_BASE_URL);
    mFirebase.child("call").child(normalizedPhone).addValueEventListener(new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {


            Log.e("dataSnapshot", dataSnapshot + "");


            if (isOnline(context)) {
                Log.e("onStartCommand", "Internet On");
            } else {
                Log.e("onStartCommand", "Internet Off");
            }


            if (dataSnapshot != null && dataSnapshot.getValue() != null && !dataSnapshot.getValue().toString().equals("--")) {
                Log.e(TAG, "Data Added " + dataSnapshot.getKey() + " with value " + dataSnapshot.getKey());
                String caller = dataSnapshot.getValue().toString();
                if (caller != null) {


                    try {
                        timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                --timer_value;
                                Log.e("Thread", "Thread = " + timer_value);
                                if (timer_value == 0) {
                                    timer.cancel();
                                    calling = true;
                                    timer_value = 10;
                               }

                            }
                        }, 0, 1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (calling) {
                        calling = false;
                        Log.e("call", "callingg start");
                        onCallFrom(CallListenerService.this, caller);
                        calling = true;
                    }

                }
            } else
                Log.e(TAG, "Aiven Hi");
            mFirebase.child("call").child(normalizedPhone).setValue("--");

        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    });

    mFirebase.child("thanks").child(normalizedPhone).addValueEventListener(new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                if (dataSnapshot != null && dataSnapshot.getValue() != null && !dataSnapshot.getValue().toString().equals("--")) {
                    Log.e(TAG, "Thanks Data Added " + dataSnapshot.getKey() + " with value " + dataSnapshot.getKey());
                    String caller = dataSnapshot.getKey();
                    String notification_value = dataSnapshot.getValue().toString();
                    Log.e("notification_value = ", notification_value + "");
                    if (caller != null) {
                        onThanksFrom(CallListenerService.this, caller, notification_value);
                    }
                } else
                    Log.e(TAG, "Aiven Hi thanks");
                mFirebase.child("thanks").child(normalizedPhone).setValue("--");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    });


    mFirebase.child("wait").child(normalizedPhone).addValueEventListener(new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                if (dataSnapshot != null && dataSnapshot.getValue() != null && !dataSnapshot.getValue().toString().equals("--")) {
                    Log.e(TAG, "Data Added " + dataSnapshot.getKey() + " with value " + dataSnapshot.getKey());
                    String caller = dataSnapshot.getKey();
                    String notification_value = dataSnapshot.getValue().toString();
                    if (caller != null) {


                        onWaitFrom(CallListenerService.this, caller, notification_value);
                    }
                } else
                    Log.e(TAG, "Aiven Hi wait");
                mFirebase.child("wait").child(normalizedPhone).setValue("--");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    });


    // Toast.makeText(getApplicationContext(), "onStartCommand = "+pref.getAppStatus(), Toast.LENGTH_SHORT).show();
        /*if (pref.getAppStatus().equals("on")){
            return Service.START_NOT_STICKY;
        }else{
            return Service.START_STICKY;
        }*/
}catch (Exception e){
    e.printStackTrace();
}
    return Service.START_STICKY;

    }


    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in air plan mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFirebase = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }




    private void onThanksFrom(Context context, String sender, String notification_value)  {
        try {
            String senderPhoneNumber = sender;
            Log.e(TAG, "Thanks Sender:" + senderPhoneNumber + ", message:" + "thanks");
            if (senderPhoneNumber == null) {
                return;
            }

        /*Intent callIntent = new Intent(context, RevertActivity.class);
        callIntent.putExtra(ReceiveActivity.KEY_MESSAGE,"thanks");
        callIntent.putExtra(ReceiveActivity.KEY_SENDER,senderPhoneNumber);
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(callIntent);*/

        /*pref.setNotificationId(pref.getNotificationId()+1);*/

            int index = notification_value.indexOf("-") + 1;
            String name = Utilities.getContactNameFromNumber(this, notification_value.substring(index));


       /* Toast.makeText(context, AllContactFragment.dialed_list.size()+"",Toast.LENGTH_SHORT).show();
        //String name="";
        for (int i=0; i<AllContactFragment.dialed_list.size(); i++)
        {
            Toast.makeText(context, "list "+AllContactFragment.dialed_list.get(i).get("phone"),Toast.LENGTH_SHORT).show();
            Toast.makeText(context, notification_value,Toast.LENGTH_SHORT).show();
            Log.e("Caller number list = ", AllContactFragment.dialed_list.get(i).get("phone")+"");
            Log.e("Caller number sender = ", sender+"");
            if(AllContactFragment.dialed_list.get(i).get("phone").equals(sender))
            {
               // name = AllContactFragment.dialed_list.get(i).get("phone");
            }
        }*/

            String arr[] = notification_value.split("-");
            //String newCountryCode = arr[0];
            String newPhoneNumber = arr[1];

            pref.setNotificationId(Integer.parseInt(newPhoneNumber.substring(1, 5)));
            Log.e("NotificationId thanks= ", pref.getNotificationId() + "");
            Log.e("Caller Name thanks= ", name + "");

            reciveCustomNotification(context, name, "Thanks alot", pref.getNotificationId());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void onWaitFrom(Context context, String sender, String notification_value){
try {
    String senderPhoneNumber = sender;
    Log.e(TAG, "Wait Sender:" + senderPhoneNumber + ", message:" + "wait");
    if (senderPhoneNumber == null) {
        return;
    }
        /*Intent callIntent = new Intent(context, RevertActivity.class);
        callIntent.putExtra(ReceiveActivity.KEY_MESSAGE,"Please wait");
        callIntent.putExtra(ReceiveActivity.KEY_SENDER, senderPhoneNumber);
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(callIntent);*/

        /*AllContactFragment allContactFragment = new AllContactFragment();
        allContactFragment.reciveCustomNotification("Please wait");*/

    //pref.setNotificationId(pref.getNotificationId() + 1);

    int index = notification_value.indexOf("-") + 1;
    String name = Utilities.getContactNameFromNumber(this, notification_value.substring(index));


    String arr[] = notification_value.split("-");
    //String newCountryCode = arr[0];
    String newPhoneNumber = arr[1];

    pref.setNotificationId(Integer.parseInt(newPhoneNumber.substring(1, 5)));
    Log.e("NotificationId wait = ", pref.getNotificationId() + "");
    Log.e("Caller Name wait= ", name + "");

    reciveCustomNotification(context, name, "Please wait", pref.getNotificationId());
}catch (Exception e){
    e.printStackTrace();
}
    }
    private void onCallFrom(Context context,String sender) {
        try {
            String senderPhoneNumber = sender;
            Log.e(TAG, "Sender:" + senderPhoneNumber + ", message:" + "call");
            if (senderPhoneNumber == null) {
                return;
            }
            Intent callIntent = new Intent(context, ReceiveActivity.class);
            callIntent.putExtra(ReceiveActivity.KEY_MESSAGE, "call");
            callIntent.putExtra(ReceiveActivity.KEY_SENDER, senderPhoneNumber);
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(callIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }






    public void reciveCustomNotification(Context context,String name, String status, int sender) {
        try {
            Bitmap icon1 = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setAutoCancel(true)
                    .setContentTitle(name)
                    .setSmallIcon(getNotificationIcon())
                    .setLargeIcon(icon1)
                    .setContentText(status);

            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
            bigText.bigText(status);
            bigText.setBigContentTitle(name);
            mBuilder.setStyle(bigText);

            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);

            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(alarmSound);

            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(sender, mBuilder.build());


        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }






    private String getContactName(Context context, String number) {

        String name = null;

        // define the columns I want the query to return
        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        // encode the phone number and build the filter URI
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        // query time
        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {
                name =      cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                Log.v(TAG, "Started uploadcontactphoto: Contact Found @ " + number);
                Log.v(TAG, "Started uploadcontactphoto: Contact name  = " + name);
            } else {
                Log.v(TAG, "Contact Not Found @ " + number);
            }
            cursor.close();
        }
        return name;
    }


}
