package com.bedees.android.receive;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bedees.android.service.CallListenerService;
import com.bedees.android.ui.ReceiveActivity;
import com.bedees.android.ui.RevertActivity;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

/**
 * Created by MOHIT on 02-01-2016.
 */
public class PushReceiver extends ParsePushBroadcastReceiver {
    public static final String TAG = PushReceiver.class.getSimpleName();

    @Override
    protected Notification getNotification(Context context, Intent intent) {
        return null;
    }

    @Override
    protected Bitmap getLargeIcon(Context context, Intent intent) {
        return super.getLargeIcon(context, intent);
    }

    @Override
    protected int getSmallIconId(Context context, Intent intent) {
        return super.getSmallIconId(context, intent);
    }

    @Override
    protected Class<? extends Activity> getActivity(Context context, Intent intent) {
        return super.getActivity(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
       // super.onPushReceive(context, intent);
        Toast.makeText(context,"onPushReceive ",Toast.LENGTH_LONG).show();
        Log.e(TAG, "OnPushReceive");
        Log.e(TAG, "type:" + intent.getType() + " ");
        Bundle bundle =intent.getExtras();
        Set<String> set =bundle.keySet();
        Log.e(TAG," Set "+set);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
       // super.onReceive(context, intent);
         Intent intent11= new Intent(context, CallListenerService.class);
        context.startService(intent11);
        Toast.makeText(context,"onReceive ",Toast.LENGTH_LONG).show();
        Log.e(TAG, "OnReceive intent  type:" + intent.getType() + " ");
        Bundle bundle =intent.getExtras();
        Set<String> set =bundle.keySet();
        String data = bundle.getString("com.parse.Data");
        Log.e(TAG," data "+data);

        try{
            JSONObject object = new JSONObject(data);

            String message = object.getString("message");//call, thanks ,wait

            Log.e(TAG,"Message received  "+message);
            if(message != null){
                if(message.equals("call")){
                    onCallFrom(context,object);
                } else if(message.equals("thanks")){
                    onThanksFrom(context,object);
                } else if(message.equals("wait")){
                    onWaitFrom(context,object);
                }
            }

        }catch (Exception exp){
            Log.e(TAG,"Exception exp "+exp);
        }
    }

    private void onThanksFrom(Context context,JSONObject object) throws JSONException{
        String senderPhoneNumber =object.getString("sender");
        if(senderPhoneNumber ==  null  ){
            Log.e(TAG,"Sender:"+senderPhoneNumber+", message:"+"thanks");
            return;
        }
        Intent callIntent = new Intent(context, RevertActivity.class);
        callIntent.putExtra(ReceiveActivity.KEY_MESSAGE,"thanks");
        callIntent.putExtra(ReceiveActivity.KEY_SENDER,senderPhoneNumber);
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(callIntent);

    }

    private void onWaitFrom(Context context, JSONObject object) throws JSONException {

        String senderPhoneNumber =object.getString("sender");
        if(senderPhoneNumber ==  null  ){
            Log.e(TAG,"Sender:"+senderPhoneNumber+", message:"+"wait");
            return;
        }
        Intent callIntent = new Intent(context, RevertActivity.class);
        callIntent.putExtra(ReceiveActivity.KEY_MESSAGE,"wait");
            callIntent.putExtra(ReceiveActivity.KEY_SENDER,senderPhoneNumber);
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(callIntent);
    }
    private void onCallFrom(Context context,JSONObject object) throws JSONException {
        String senderPhoneNumber =object.getString("sender");
        if(senderPhoneNumber ==  null  ){
            Log.e(TAG,"Sender:"+senderPhoneNumber+", message:"+"call");
            return;
        }
        Intent callIntent = new Intent(context, ReceiveActivity.class);
        callIntent.putExtra(ReceiveActivity.KEY_MESSAGE,"call");
        callIntent.putExtra(ReceiveActivity.KEY_SENDER,senderPhoneNumber);
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(callIntent);

    }


    public PushReceiver() {
        super();
    }
}
