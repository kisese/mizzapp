package com.bedees.android.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.app.NotificationCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bedees.android.R;
import com.bedees.android.adapter.CatFactAdapter;
import com.bedees.android.adapter.CricketMatchAdapter;
import com.bedees.android.application.Application;
import com.bedees.android.item.CatFact;
import com.bedees.android.item.CricketItem;
import com.bedees.android.util.Api;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by mk on 4/24/2016.
 */
public class CatFactFragment extends Fragment implements View.OnClickListener{

    private boolean check_backbtn = false;
    private View view;
    private EditText txt_search;
    private ListView listview;
    private ProgressDialog progressDialog;
    private ArrayList<CatFact> cricket_list;
    private ArrayList<CatFact> list_search;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cricketmatch, container, false);
        ConnectivityManager cn=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        txt_search = (EditText) view.findViewById(R.id.txt_search);
        listview = (ListView) view.findViewById(R.id.listview);
        if(nf != null && nf.isConnected()==true )
        {
            MatchApi();
        }
        else
        {
            Toast.makeText(getActivity(), "Network Not Available", Toast.LENGTH_SHORT).show();
        }

        txt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!txt_search.getText().toString().equals("")) {
                    check_backbtn = false;
                    Search(txt_search.getText().toString());
                } else {
                    try {
                        check_backbtn = true;
                        CatFactAdapter cricketMatchAdapter = new CatFactAdapter(getActivity(), cricket_list, CatFactFragment.this);
                        listview.setAdapter(cricketMatchAdapter);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });




        ((ContactActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ContactActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        ContactActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().popBackStack();
            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {

        }
    }



    public void MatchApi() {

        progressDialog.show();
        RequestQueue queue = Application.getInstance().getRequestQueue();

        Log.e("cricket_url", Api.CRICKET + "");

        StringRequest myReq = new StringRequest(Request.Method.GET, Api.Catfact,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Cricket Resoponse", response + "..");

                        try {

                            JSONObject json = new JSONObject(response);

                            JSONArray jsonArray = json.getJSONArray("facts");
                         ArrayList<String> arrayList= new ArrayList<>();
                            cricket_list = new ArrayList<>();
                            for (int i=0; i<jsonArray.length(); i++) {
                                arrayList.add(i,jsonArray.getString(i));

                            }
                            CatFact catFact= new CatFact();
                            catFact.setDescription(arrayList.toString());
                            catFact.setSucess(json.getString("success"));
                            cricket_list.add(catFact);
                            CatFactAdapter cricketMatchAdapter = new CatFactAdapter(getActivity(),cricket_list, CatFactFragment.this);
                            listview.setAdapter(cricketMatchAdapter);
                            progressDialog.dismiss();

                        } catch (Exception e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {

                Log.e("Resoponse errror", arg0 + "..");
                Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        queue.add(myReq);


    }


    public void MatchScoreApi(final String match_id) {

        progressDialog.show();
        RequestQueue queue = Application.getInstance().getRequestQueue();

        StringRequest myReq = new StringRequest(Request.Method.GET, Api.Catfact,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Score Resoponse", response + "..");

                        try {
                            JSONObject json = new JSONObject(response);

                            JSONArray jsonArray = json.getJSONArray("facts");
                            ArrayList<String> arrayList= new ArrayList<>();
                            cricket_list = new ArrayList<>();
                            for (int i=0; i<jsonArray.length(); i++) {
                                arrayList.add(i,jsonArray.getString(i));

                            }

                           json.getString("success");

                             String score =  arrayList.toString();
                            Log.e("score", score);

                            reciveCustomNotification(score, 1);
                            Toast.makeText(getActivity(),"Please check score notification", Toast.LENGTH_SHORT).show();

                            progressDialog.dismiss();

                        } catch (Exception e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {

                Log.e("Resoponse errror", arg0 + "..");
                Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        queue.add(myReq);


    }






/*============================  Notification Code Start Here ==================================*/



    public void reciveCustomNotification(String score, int noti_id) {



        NotificationCompat.BigPictureStyle notiStyle = new
                NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle("Mizz Facts");
        notiStyle.setSummaryText(score);



// Add the big picture to the style.
        notiStyle.setSummaryText(score);//bigPicture(remote_picture);
        NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(getActivity()).setAutoCancel(true);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String[] events = new String[score.length()];
// Sets a title for the Inbox in expanded layout
        inboxStyle.setBigContentTitle("Mizz Facts");
// Moves events into the expanded layout
        for (int i=0; i < events.length; i++) {

            inboxStyle.addLine("Hello");
            inboxStyle.addLine("Hello1323");
            inboxStyle.addLine("Hello252");
        }
// Moves the expanded layout object into the notification object.
        mBuilder1.setStyle(inboxStyle);



        Bitmap icon1 = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);



        Notification foregroundNote;

        RemoteViews bigView = new RemoteViews(getActivity().getPackageName(),
                R.layout.notification_layout_big);

// bigView.setOnClickPendingIntent() etc..

        Notification.Builder mNotifyBuilder = new Notification.Builder(getActivity());
        foregroundNote = mNotifyBuilder.setContentTitle("Mizz Score")
                .setContentText(score)
                .setSmallIcon(getNotificationIcon())
                .setLargeIcon(icon1)

                .build();

        foregroundNote.bigContentView = bigView;

// now show notification..
        NotificationManager mNotifyManager = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyManager.notify(1, foregroundNote);





        Intent resultIntent = new Intent(getActivity(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mNotifyBuilder.setContentIntent(resultPendingIntent);


        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mNotifyBuilder.setSound(alarmSound);

        NotificationManager mNotificationManager = (NotificationManager) getActivity()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(noti_id, mNotifyBuilder.build());


    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }



    public void Search(String keyword) {

        list_search = new ArrayList<>();

        Log.e("list_search keyword", keyword + "...");
        for (int i = 0; i < cricket_list.size(); i++) {
            if (cricket_list.get(i).getSucess().toLowerCase().contains(keyword.toLowerCase())) {

                list_search.add(cricket_list.get(i));
            }
        }

        Log.e("list_search size", list_search.size() + "...");
        CatFactAdapter cricketMatchAdapter = new CatFactAdapter(getActivity(),list_search,this);
        listview.setAdapter(cricketMatchAdapter);

    }





}