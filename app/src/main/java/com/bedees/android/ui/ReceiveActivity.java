package com.bedees.android.ui;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bedees.android.R;
import com.bedees.android.drawable.WaveDrawable;
import com.bedees.android.sqlite.DBHelper;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.SavePref;
import com.bedees.android.util.Utilities;
import com.firebase.client.Firebase;
import com.skyfishjy.library.RippleBackground;

public class ReceiveActivity extends AppCompatActivity {
    public static final String TAG = ReceiveActivity.class.getSimpleName();
    public static final String KEY_MESSAGE = "key_message";
    public static final String KEY_SENDER = "key_sender";

    private boolean call_attend=false;
    public String senderNumber;
    private LinearLayout mainLinearLayout;
    private TextView nameTextView,timeTextView,numberTextView;
    private Firebase mFirebase;
    private String name;
    private Vibrator vibrator;
    private Ringtone ringtone;
    private boolean stopring=true;
    private SavePref pref;
    EditText editmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive);


       // Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_SHORT).show();

        pref = new SavePref(getApplicationContext());

        Intent intent = getIntent();
        mFirebase = new Firebase(FirebaseConstants.FIRE_BASE_URL);
        editmsg=(EditText)findViewById(R.id.editmsg);
        mainLinearLayout = (LinearLayout)findViewById(R.id.ll_main);
        LinearInterpolator interpolator = new LinearInterpolator();
        WaveDrawable drawable =  new WaveDrawable(Color.argb(100,220,220,220), 500);
       // drawable.setWaveInterpolator(interpolator);
     //   drawable.startAnimation();
        mainLinearLayout.setBackgroundDrawable(drawable);

        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);
        rippleBackground.startRippleAnimation();

        if(intent.hasExtra(KEY_MESSAGE)&&intent.hasExtra(KEY_SENDER)){
            if(intent.getStringExtra(KEY_MESSAGE).toLowerCase().equals("call")){
                nameTextView = (TextView)findViewById(R.id.tv_name);
                timeTextView = (TextView)findViewById(R.id.tv_time);
                numberTextView = (TextView)findViewById(R.id.tv_phone_number);

                senderNumber = intent.getStringExtra(KEY_SENDER);
                numberTextView.setText(senderNumber);
                int index = senderNumber.indexOf("-")+1;
try {
    name = Utilities.getContactNameFromNumber(this, senderNumber.substring(index));
    Log.e(TAG, "Name : " + name);
    nameTextView.setText(name);

    //unlock screen
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
            | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

    //vibrate
    //Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
    //vibrator.vibrate(new long[]{0, 500, 110, 500, 110, 450, 110, 200, 110, 170, 40, 450, 110, 200, 110, 170, 40, 500}, -1);

    Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

    AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

    if (am.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
        stopring = false;
        //Toast.makeText(getApplicationContext(), "vibrate", Toast.LENGTH_SHORT).show();
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(new long[]{0, 500, 110, 500, 110, 450, 110, 200, 110, 170, 40, 450, 110, 200, 110, 170, 40, 500}, -1);

    } else if (am.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
        //Toast.makeText(getApplicationContext(), "silent", Toast.LENGTH_SHORT).show();
    } else {
        stopring = true;
        //Toast.makeText(getApplicationContext(), "tone", Toast.LENGTH_SHORT).show();
        ringtone = RingtoneManager.getRingtone(getApplicationContext(), uri);
        ringtone.play();
    }


    new CountDownTimer(10500, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            timeTextView.setText(String.format("00:%02d", ((int) millisUntilFinished / 1000)));
        }

        @Override
        public void onFinish() {
            Log.e(TAG, "Timer Up");
            finish();
        }
    }.start();
}catch (Exception e){
    e.printStackTrace();
}

            }else{
                Log.e(TAG,"Intent doesnt have call as a message");
                finish();
            }


        }else{
            Log.e(TAG," Intent doesn't have message key or sender key");
            finish();
        }




    }


    public void onThanksClick(View view){
        //Send Push

        try {
            if (stopring)
            {
                ringtone.stop();
            }else{
                vibrator.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        call_attend=true;
        String sender = Utilities.getVerifiedNormalizedPhoneNumber(this);
        String sendervalue= editmsg.getText().toString();
        mFirebase.child("thanks"+sendervalue).child(this.senderNumber).setValue(sender);

        finish();
      /*  HashMap<String,String> params = new HashMap<>();
        params.put(ParseConstants.KEY_PHONE_NUMBER, senderNumber);
        params.put(ParseConstants.KEY_SENDER, ParseInstallation.getCurrentInstallation().getString(ParseConstants.KEY_PHONE_NUMBER));
        params.put(ParseConstants.KEY_MESSAGE,"thanks");
        Log.e(TAG,params.get(ParseConstants.KEY_SENDER) +" thanks  "+params.get(ParseConstants.KEY_PHONE_NUMBER));*/
      /*  ParseCloud.callFunctionInBackground(ParseConstants.FUNCTION_SEND_PUSH, params, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {
                Toast.makeText(ReceiveActivity.this, "thanks sent to  "+"", Toast.LENGTH_SHORT).show();
                if(e == null){
                    Log.e(TAG,"Push Success");
                }else{
                    Log.e(TAG,"Push Failed due to exception "+e.toString());
                }
            }
        });*/
    }

    public void onWaitClick(View view){
        //Send Push

        try {
            if (stopring)
            {
                ringtone.stop();
            }else{
                vibrator.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        call_attend=true;
        String sender = Utilities.getVerifiedNormalizedPhoneNumber(this);
        mFirebase.child("wait").child(this.senderNumber).setValue(sender);
        finish();
/*
        HashMap<String,String> params = new HashMap<>();
        params.put(ParseConstants.KEY_PHONE_NUMBER, senderNumber);
        params.put(ParseConstants.KEY_SENDER, ParseInstallation.getCurrentInstallation().getString(ParseConstants.KEY_PHONE_NUMBER));
        params.put(ParseConstants.KEY_MESSAGE,"wait");
        Log.e(TAG,params.get(ParseConstants.KEY_SENDER) +" wait  "+params.get(ParseConstants.KEY_PHONE_NUMBER));
*/
       /* ParseCloud.callFunctionInBackground(ParseConstants.FUNCTION_SEND_PUSH, params, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {
                Toast.makeText(ReceiveActivity.this, "wait sent to  "+"", Toast.LENGTH_SHORT).show();
                if(e == null){
                    Log.e(TAG,"Push Success");
                }else{
                    Log.e(TAG,"Push Failed due to exception "+e.toString());
                }
            }
        });*/
    }


    @Override
    protected void onStop() {
        super.onStop();
try {
    if (!call_attend) {
        String arr[] = senderNumber.split("-");
        String newCountryCode = arr[0];
        String newPhoneNumber = arr[1];

        //pref.setNotificationId(pref.getNotificationId()+1);
        pref.setNotificationId(Integer.parseInt(newPhoneNumber.substring(1, 5)));
        Log.e("miss call noti = ", pref.getNotificationId() + "");

        DBHelper helper = new DBHelper(getApplicationContext());
        if (name.equals("Unknown")) {
            helper.callMissed(newPhoneNumber, senderNumber, newCountryCode);
            reciveCustomNotification(senderNumber, pref.getNotificationId());
        } else {
            helper.callMissed(newPhoneNumber, name, newCountryCode);
            reciveCustomNotification(name, pref.getNotificationId());
        }


        try {
            if (stopring) {
                ringtone.stop();
            } else {
                vibrator.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}catch (Exception e){
    e.printStackTrace();
}
        //Toast.makeText(getApplicationContext(), "onStop", Toast.LENGTH_SHORT).show();

        //ExitActivity.exitApplication(ReceiveActivity.this);
    }

    public void reciveCustomNotification(String caller_name, int caller_number) {

        Bitmap icon1 = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ReceiveActivity.this).setAutoCancel(true)
                .setContentTitle("Missed Call")
                .setSmallIcon(getNotificationIcon())
                .setLargeIcon(icon1)
                .setContentText(caller_name);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(caller_name);
        bigText.setBigContentTitle("Missed Call");
        mBuilder.setStyle(bigText);

        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);

        NotificationManager mNotificationManager = (NotificationManager) ReceiveActivity.this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(caller_number, mBuilder.build());


    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }


   /* @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(getApplicationContext(), "onResume", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(getApplicationContext(), "onPause", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Toast.makeText(getApplicationContext(), "onRestart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (pref.getAppStatus().equals("0"))
        {

        }

        Toast.makeText(getApplicationContext(), "onStart", Toast.LENGTH_SHORT).show();
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(pref.getAppStatus().equals("0")) {
            Log.e("onDestroy", "onDestroy calling");
            /*Toast.makeText(getApplicationContext(), "onDestroy", Toast.LENGTH_SHORT).show();*/
            /*moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);*/

            /*Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);

            Intent home = new Intent(this, MainActivity.class);
            home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(home);
            finish();*/

            Intent intent = new Intent(getApplicationContext(), ContactActivity.class);
            ComponentName cn = intent.getComponent();
            Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
            startActivity(mainIntent);
            finish();




           /* Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);*/
        }

    }

   /* @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Toast.makeText(getApplicationContext(), "onPostCreate", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Toast.makeText(getApplicationContext(), "onPostResume", Toast.LENGTH_SHORT).show();
    }*/


}
