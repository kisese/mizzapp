package com.bedees.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bedees.android.R;

/**
 * Created by mk on 4/24/2016.
 */
public class ExploreMizzFragment extends Fragment implements View.OnClickListener{

    private Intent intent;
    private View view;
    private RelativeLayout layout_cricket_score,layout_business_help,layout_polls;

    public static ExploreMizzFragment newInstance() {
        ExploreMizzFragment fragment = new ExploreMizzFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exploremizz, container, false);

        layout_cricket_score = (RelativeLayout) view.findViewById(R.id.layout_cricket_score);
        layout_business_help = (RelativeLayout) view.findViewById(R.id.layout_business_help);
        layout_polls = (RelativeLayout) view.findViewById(R.id.layout_polls);

        layout_cricket_score.setOnClickListener(this);
        layout_business_help.setOnClickListener(this);
        layout_polls.setOnClickListener(this);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((ContactActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((ContactActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.layout_cricket_score:

                FragmentTransaction trans = getFragmentManager().beginTransaction();
                trans.replace(R.id.root_frame, new CricketMatchFragment());
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();



                break;

            case R.id.layout_business_help:
                FragmentTransaction catfact = getFragmentManager().beginTransaction();
                catfact.replace(R.id.root_frame, new CatFactFragment());
                catfact.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                catfact.addToBackStack(null);
                catfact.commit();
                break;

            case R.id.layout_polls:
                FragmentTransaction trans1 = getFragmentManager().beginTransaction();
                trans1.replace(R.id.root_frame, new FAVFragment());
                trans1.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans1.addToBackStack(null);
                trans1.commit();
                break;

        }
    }
}
