package com.bedees.android.ui;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.model.Contact;
import com.bedees.android.util.DownloadStatus;
import com.bedees.android.util.Utilities;
import com.bumptech.glide.Glide;

import java.io.File;

/**
 * Used to show profile popup
 */
public class ProfilePopupDialog extends DialogFragment {

    private Contact mContact;
    private String voiceStoragePath;
    private Uri mFileUri;
    private DownloadStatus downloadStatus;
    private MediaPlayer mediaPlayer;

    public static ProfilePopupDialog newInstance(Contact c) {
        ProfilePopupDialog fragment = new ProfilePopupDialog();
        fragment.setContact(c);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_profile_popup, container, false);
        if (mContact != null) {
            showProfilePicture(view);
            showContactName(view);
            downloadStatus = new DownloadStatus(getActivity());
            voiceStoragePath = voiceStoragePath + File.separator + "voices/user" + mContact.getNormalizedNumber() + ".mp3";
            File file = new File(voiceStoragePath);
            mFileUri = Uri.fromFile(file);

            //beginDownload();
            Log.e("phone number", mContact.getNormalizedNumber());
        }

        return view;
    }

    private void beginDownload() {
        // Get path
        String path = "voices/" + mFileUri.getLastPathSegment();

        // Kick off download service
        Intent intent = new Intent(getActivity(), MyDownloadService.class);
        intent.setAction(MyDownloadService.ACTION_DOWNLOAD);
        intent.putExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH, path);
        getActivity().startService(intent);

        Toast.makeText(getActivity(), "Downloading voice", Toast.LENGTH_LONG).show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                try {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(voiceStoragePath);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    Log.e("Playing", "Playing");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 3500);

        downloadStatus.setDownloadStatus(true);
    }


    private String generateVoiceFilename() {
        return Utilities.getVerifiedNormalizedPhoneNumber(getActivity());
    }

    /**
     * Displays Contact picture in ImageView via Glide
     */
    private void showProfilePicture(View view) {
        ImageView image = (ImageView) view.findViewById(R.id.iv_profile_picture_dialog);
        if (!TextUtils.isEmpty(mContact.getImageUrl())) {
            Glide.with(getContext())
                    .load(mContact.getImageUrl())
                    .into(image);
        }
    }

    /**
     * Displays Contact name in top TextView
     */
    private void showContactName(View view) {
        TextView tvName = (TextView) view.findViewById(R.id.tv_contact_name);
        if (!TextUtils.isEmpty(mContact.getName())) {
            tvName.setText(mContact.getName());
        }
    }

    public void setContact(Contact mContact) {
        this.mContact = mContact;
    }
}
