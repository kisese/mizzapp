package com.bedees.android.ui;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.service.CallListenerService;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.SavePref;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.List;

public class ContactActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    public static Toolbar toolbar;
    private TabLayout tabLayout;
    private SavePref pref;
    //private SectionsPagerAdapter mSectionsPagerAdapter;
    //public static boolean AppOn=true;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private FloatingActionButton floatingActionButton;

    public static final String TAG= ContactActivity.class.getSimpleName();




    private Firebase mFireBase;

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 123;
    private void requestPermission(){
        if (ContextCompat.checkSelfPermission(this,
                "android.permission.READ_CONTACTS")
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    "android.permission.READ_CONTACTS")) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{"android.permission.READ_CONTACTS"},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(ContactActivity.this, "Why dont you gave me permission?", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        pref = new SavePref(ContactActivity.this);
        pref.setAppStatus("1");
        //Toast.makeText(getApplicationContext(), pref.getAppStatus(), Toast.LENGTH_SHORT).show();

        //floatingActionButton = (FloatingActionButton)findViewById(R.id.fab);
        final Intent intent= new Intent(this, CallListenerService.class);
        startService(intent);

        if (pref.getNotificationId() >99)
        {
            pref.setNotificationId(0);
        }
        //Toast.makeText(getApplicationContext(), pref.getNotificationId()+"", Toast.LENGTH_SHORT).show();


        requestPermission();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mFireBase = new Firebase(FirebaseConstants.FIRE_BASE_URL);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        //mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

      //  mViewPager.setAdapter(mSectionsPagerAdapter);


        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.getTabAt(3).setIcon(R.mipmap.ic_explore).setText("Explore");
        tabLayout.getTabAt(2).setIcon(R.mipmap.ic_contact).setText("Contact");
        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_dialed).setText("Dialled");
        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_incoming).setText("Missed");


       /* floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    openShare();
                } catch (Exception exp) {
                    Log.e(TAG, "Exception while adding new contact " + exp.toString());
                }
            }
        });*/



        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                if (position==3) {

                    FragmentManager fm = getSupportFragmentManager();
                    for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }

                }


            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Why not use resources Strings???
        adapter.addFragment(new IncomingFragment(), "Missed");
        adapter.addFragment(new DialledFragment(), "Dialled");
        adapter.addFragment(new AllContactFragment(), "Contact");
        adapter.addFragment(new RootFragment(), "Explore Mizz");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {

            super(manager);
        }


        @Override
        public Fragment getItem(int position) {


            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {


            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);


        }

        @Override
        public CharSequence getPageTitle(int position) {


            return mFragmentTitleList.get(position);
        }
    }























    private void openShare() {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Hey.! I just sent you a Mizz on Mizz Call App \uD83D\uDE0A Send me Mizz when you miss me..\n" +
                "\n" +
                "Click on the link to download\n" +
                "https://play.google.com/store/apps/details?id=com.bedees.android\n" +
                "Share and Download Mizz call app.";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void addPhoneAddListener(){
        mFireBase.child("phone").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG,"Child Added at"+dataSnapshot.getRef()+" with key "+dataSnapshot.getKey()+"  with value "+dataSnapshot.getValue() + " type "+dataSnapshot.getClass());
                onNewInstallationAdded(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


    private void onNewInstallationAdded(String newNumber){
        String arr[] = newNumber.split("-");
        String newCountryCode = arr[0];
        String newPhoneNumber = arr[1];
        String[] projection = new String[]{
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID
        };
        Uri contactUri= Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(newPhoneNumber));
        ContentResolver cr = getContentResolver();
        Cursor crc= cr.query(contactUri, projection, null, null, null);
        String name;
        if(crc.moveToNext()){
            name=crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));

        }   else{
            name ="Unknown";
            Log.e(TAG,"New Installation not known");
        }
        Log.e(TAG,"New Installation has name  "+name+" with phone number "+newPhoneNumber);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent= new Intent(getApplicationContext(),ProfileActivity.class);
            startActivity(intent);


            //return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_contact, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position){
                case 3:
                    return ExploreMizzFragment.newInstance();
                case 2:
                    return AllContactFragment.newInstance();
                case 1:
                    return DialledFragment.newInstance();
                case 0:
                    return IncomingFragment.newInstance();

            }
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Contact";
                case 1:
                    return "Dialled";
                case 2:
                    return "Missed";
                case 3:
                    return "Explore Mizz";
            }
            return null;
        }

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();


          pref.setAppStatus("0");
        //Toast.makeText(getApplicationContext(), "onDestroy = "+pref.getAppStatus(), Toast.LENGTH_SHORT).show();





     //   Toast.makeText(getApplicationContext(), "App Off", Toast.LENGTH_SHORT).show();

        /*Intent intent= new Intent(this, CallListenerService.class);
        startService(intent);*/




        Log.e("onDestroy", "onDestroy");
    }
}
