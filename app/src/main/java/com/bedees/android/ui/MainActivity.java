package com.bedees.android.ui;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bedees.android.R;
import com.bedees.android.util.ParseConstants;
import com.bedees.android.util.SavePref;
import com.bedees.android.util.Utilities;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class MainActivity extends AppCompatActivity {

    //8449880092 verifiy number
    //8475981191

    public static final String TAG = MainActivity.class.getSimpleName();

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROGRESS_TITLE_VERIFYING_PHONE_NUMBER = "Verifying your phone number.";
    public static final String PROGRESS_TITLE_REGISTRATION = "Saving your profile.";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static HashMap<String, String> countryCodeHashMap = new HashMap();
    private BroadcastReceiver messageReceivedBroadcastReceiver;
    private String mCurrentVerificationCode = "";
    private String mCurrentEnteredPhoneNumber = "";
    private EditText mPhoneNumberEditText;
    private ProgressDialog mProgressDialog;
    private SavePref pref;

    private String mCountryCallingCode;

    private void checkIfAppRegistered() {
        if (Utilities.isAppRegistered(this)) {

            try {
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotificationManager.cancelAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            startActivity(new Intent(this, ContactActivity.class));
            this.finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = new SavePref(MainActivity.this);
        checkIfAppRegistered();
        mPhoneNumberEditText = (EditText) findViewById(R.id.et_phone_number);
        mCountryCallingCode = getCountryCallingCode();
        ((TextView) findViewById(R.id.tv_country_code)).setText(mCountryCallingCode);
        registerReceiver();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    protected void registerReceiver() {
        Log.i(TAG, "register receiver");
        try {
            messageReceivedBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle extras = intent.getExtras();
                    String messageBody = "";
                    String sender = "";
                    Object[] pdus = (Object[]) extras.get("pdus");
                    for (int i = 0; i < pdus.length; i++) {
                        SmsMessage smsReceived = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        sender = smsReceived.getOriginatingAddress();
                        String body = smsReceived.getMessageBody().toString();

                        if (body != null)
                            messageBody += body;
                    }
                    Log.i(TAG, "messsageReceiveBroadcastReceiver messageBody " + messageBody + " sender " + sender);
                    if (!messageBody.isEmpty() && !sender.isEmpty()) {
                        if (messageBody.equals(mCurrentVerificationCode) && PhoneNumberUtils.compare(sender, mCurrentEnteredPhoneNumber)) {
                            Log.i(TAG, "Phone Number Verified Verification Code= " + mCurrentVerificationCode + " Phone Number" + mCurrentEnteredPhoneNumber);

                            Utilities.saveVerifiedPhoneNumber(context, mCurrentEnteredPhoneNumber, mCountryCallingCode);

                            setProgressDialogStatus(PROGRESS_TITLE_REGISTRATION);
                            abortBroadcast();
                            registerInBackground();
                        } else {

                            Utilities.makeToastLong(MainActivity.this, "Verification Failed.");
                            dismissProgressDialog();
                        }
                    } else {
                        Log.i(TAG, " message body is empty or sender is empty " + messageBody + " sender " + sender);
                    }

                }
            };
            if (messageReceivedBroadcastReceiver != null) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
                intentFilter.setPriority(18);
                registerReceiver(messageReceivedBroadcastReceiver, intentFilter);

            }

        } catch (Exception e) {
            Log.e(TAG, "Exception While Registering the receiver.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (messageReceivedBroadcastReceiver != null) {
                unregisterReceiver(messageReceivedBroadcastReceiver);
                Log.i(TAG, "unregister receiver");
            }
        } catch (Exception e) {
            Log.i(TAG, "Exception in OnPause", e);
        }
    }

    private void showProgressDialog() {
        try {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage(PROGRESS_TITLE_VERIFYING_PHONE_NUMBER);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            Log.i(TAG, "Progress Dialog Shown");
        } catch (Exception exp) {
            Log.e(TAG, "Exception ");
        }
    }

    private void dismissProgressDialog() {
        Log.i(TAG, "Progress Dialog Dismissed.");
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private void setProgressDialogStatus(String newStatus) {
        Log.i(TAG, "Progress Dialog status changed to " + newStatus);
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.setMessage(newStatus);
    }

    private boolean checkConnectivity() {

        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (!isConnected) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Connect to network or quit")
                    .setCancelable(false)
                    .setPositiveButton("Connect to Network", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    })
                    .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            MainActivity.this.finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        Log.i(TAG, " Connectivity Checked Status:" + isConnected);
        return isConnected;
    }

    private String getCountryCallingCode() {
        /*TelephonyManager tm = (TelephonyManager) getSystemService(getApplicationContext().TELEPHONY_SERVICE);
        String countryCodeIso = tm.getNetworkCountryIso();
        String countryCallingCode = "";
        //Utilities.makeToastLong(this, "ISO Code " + countryCodeIso);
        // getResources().getConfiguration().locale.toString().split("_")[1].
        if (countryCodeHashMap.containsKey(countryCodeIso))
            countryCallingCode = countryCodeHashMap.get(countryCodeIso).toString();

        return countryCallingCode;*/
        String callingCode = Utilities.getCountryZipCode(this);
        Log.i(TAG, "Country Code " + "+" + callingCode);
        return "+" + callingCode;
    }

    private boolean validatePhoneNumber(String enteredPhoneNumber) {

        if (enteredPhoneNumber == null || enteredPhoneNumber.isEmpty()) {
            Utilities.makeToastLong(this, "Please Enter Valid Phone Number");
            return false;
        }
        if ((enteredPhoneNumber.length() < 3) || (!PhoneNumberUtils.isWellFormedSmsAddress(enteredPhoneNumber))) {
            Utilities.makeToastLong(this, "Please Enter Valid Phone Number");
            return false;
        }
        return true;
    }


    //GCM

    private void sendVerificationSMS(String phoneNumber) {
        SmsManager smsManager = SmsManager.getDefault();
        String verificationCode = Utilities.generateAndSaveVerificationCode(this);
        Log.i(TAG, "Verification Code Generated in SendVerificationSMS " + verificationCode);
        mCurrentVerificationCode = verificationCode;
        mCountryCallingCode = getCountryCallingCode();
        smsManager.sendTextMessage(mCountryCallingCode + phoneNumber, null, verificationCode, null, null);
    }

    private void registerInBackground() {
        if (checkConnectivity()) {

            String phoneNumber = Utilities.getVerifiedPhoneNumber(this);
            String countryCode = Utilities.getVerifiedCountryCode(this);
            ParseObject user = ParseObject.create(ParseConstants.CLASS_PHONE);
            user.put(ParseConstants.KEY_COUNTRY_CODE, countryCode);
            user.put(ParseConstants.KEY_PHONE_NUMBER, countryCode + "-" + phoneNumber);

            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            //installation.put(ParseConstants.KEY_USER, user);
            installation.put(ParseConstants.KEY_PHONE_NUMBER, countryCode + "-" + phoneNumber);
            installation.put(ParseConstants.KEY_COUNTRY_CODE, countryCode);
            installation.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        dismissProgressDialog();
                        Utilities.appRegistered(getApplicationContext());
                        startActivity(new Intent(MainActivity.this, ContactActivity.class));
                        MainActivity.this.finish();
                    } else {
                        Utilities.makeToastLong(MainActivity.this, "Registration Failed");
                        Log.e(TAG, "Registration Failed due to " + e.toString());
                    }

                }
            });
            user.saveInBackground();

        }
    }

    private void requestVerificationSMS() {
        Log.e(TAG, "Now sending code to " + mCountryCallingCode + mCurrentEnteredPhoneNumber);

        //client credantials
        String authId = "MAODA3YJFMNTEZYMNLY2";
        String authToken = "NDFiOGUwMWE1YTZjYjhkOTc5ZmZkZDc2OWI1N2My";

        //String authId = "MANDU3ZJMWN2FIZDDKOG";
        //String authToken = "OTNiNWEzNDcwMWZjZjNiZDZjMzUwMDg3MWExZjBm";


        RestAPI api = new RestAPI(authId, authToken, "v1");

        LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
        parameters.put("src", "MizzCall"); // Sender's phone number with country code
        parameters.put("dst", mCountryCallingCode + mCurrentEnteredPhoneNumber); // Receiver's phone number with country code
        parameters.put("text", "Your Mizz call App verification code is " + Utilities.generateAndSaveVerificationCode(this)); // Your SMS text message


        // Send Unicode text
        //parameters.put("text", "こんにちは、元気ですか？"); // Your SMS text message - Japanese
        //parameters.put("text", "Ce est texte généré aléatoirement"); // Your SMS text message - French
        parameters.put("url", "http://example.com/report/"); // The URL to which with the status of the message is sent
        parameters.put("method", "GET"); // The method used to call the url


        try {
            // Send the message
            MessageResponse msgResponse = api.sendMessage(parameters);
            Log.e("msgResponse", msgResponse + "");

            // Print the Api ID
            Log.e(TAG, "Api ID : " + msgResponse.apiId);
            // Print the Response Message
            Log.e(TAG, "Message : " + msgResponse.message);

            if (msgResponse.serverCode == 202) {
                // Print the Message UUID
                Log.e(TAG, "Message UUID : " + msgResponse.messageUuids.get(0).toString());
            } else {
                //       Toast.makeText(MainActivity.this, "Sending SMS Failed", Toast.LENGTH_SHORT).show();
                Log.e(TAG, msgResponse.error);
            }
        } catch (PlivoException e) {

            e.printStackTrace();
            Log.e(TAG, "Plivo Exception " + e);
        }
        /*  ParseCloud.callFunctionInBackground(ParseConstants.FUNCTION_REQUEST_CODE, params, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {
                Log.e(TAG,"Object "+object +" Exception "+e);
                Intent intent = new Intent(MainActivity.this,VerifyCodeActivity.class);
                Log.e(TAG,"Phone Number : "+mCurrentEnteredPhoneNumber+",  CountryCode: "+mCountryCallingCode);
                intent.putExtra(VerifyCodeActivity.KEY_PHONE,mCurrentEnteredPhoneNumber);
                intent.putExtra(VerifyCodeActivity.KEY_COUNTRY_CODE,mCountryCallingCode);
                startActivity(intent);
                finish();

            }
        });*/
    }

    public void onVerifyClick(View view) {
        String enteredPhoneNumber = mPhoneNumberEditText.getText().toString();
        if (enteredPhoneNumber.length() > 2) {

        } else {
            return;
        }


        if (validatePhoneNumber(enteredPhoneNumber)) {
            Log.e(TAG, "Phone Number is Valid. Let's see entered phone number " + enteredPhoneNumber);
            mCurrentEnteredPhoneNumber = enteredPhoneNumber;
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    requestVerificationSMS();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    pref.setPhoneNumber(mCurrentEnteredPhoneNumber);
                    Intent intent = new Intent(MainActivity.this, VerifyCodeActivity.class);
                    Log.e(TAG, "Phone Number : " + mCurrentEnteredPhoneNumber + ",  CountryCode: " + mCountryCallingCode);
                    intent.putExtra(VerifyCodeActivity.KEY_PHONE, mCurrentEnteredPhoneNumber);
                    intent.putExtra(VerifyCodeActivity.KEY_COUNTRY_CODE, mCountryCallingCode);
                    startActivity(intent);
                    finish();


                }
            }.execute();
            // sendVerificationSMS(enteredPhoneNumber);
            showProgressDialog();
            setProgressDialogStatus(PROGRESS_TITLE_VERIFYING_PHONE_NUMBER);
        }


//        startActivity(new Intent(this,ContactActivity.class));
    }
}
