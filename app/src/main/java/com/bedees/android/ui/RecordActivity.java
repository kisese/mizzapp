package com.bedees.android.ui;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.util.AudioPlayer;
import com.bedees.android.util.Utilities;
import com.sembozdemir.speechorbview.library.SpeechOrbView;

import java.io.File;
import java.io.IOException;

public class RecordActivity extends AppCompatActivity {

    private TextView counter_text, header_prompt, action_prompt, countdown_txt;
    private SpeechOrbView speechOrbView;
    long startTime;
    long countUp;
    private Chronometer stopWatch;
    String[] presets;
    private ImageView countdown, crowd, ghost, laughing,  yabba, zombie;
    private AudioPlayer audioPlayer = new AudioPlayer();
    private String voiceStoragePath;
    private MediaRecorder mediaRecorder;
    private Uri mFileUri;
    private LinearLayout speech_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        counter_text = (TextView) findViewById(R.id.counter_text);
        header_prompt = (TextView) findViewById(R.id.text_header_prompt);
        action_prompt = (TextView) findViewById(R.id.text_action_prompt);
        speechOrbView = (SpeechOrbView) findViewById(R.id.speech_orb_view);

        countdown = (ImageView) findViewById(R.id.btn_countdown);
        crowd = (ImageView) findViewById(R.id.btn_crowd);
        ghost = (ImageView) findViewById(R.id.btn_ghost);
        laughing = (ImageView) findViewById(R.id.btn_laughing);
        yabba = (ImageView) findViewById(R.id.btn_yabba_dabba);
        countdown_txt = (TextView) findViewById(R.id.text_countdown);
        zombie = (ImageView) findViewById(R.id.btn_zombie);
        speech_layout = (LinearLayout) findViewById(R.id.speech_layout);

        stopWatch = (Chronometer) findViewById(R.id.chrono);
        startTime = SystemClock.elapsedRealtime();

        voiceStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File audioVoice = new File(voiceStoragePath + File.separator + "voices");
        if (!audioVoice.exists()) {
            audioVoice.mkdir();
        }
        voiceStoragePath = voiceStoragePath + File.separator + "voices/" + generateVoiceFilename(6) + ".mp3";

        hasSDCard();

        initializeMediaRecord();

        try {
            presets = getAssets().list("presets");
        } catch (Exception e) {
            e.printStackTrace();
        }



        action_prompt.setVisibility(View.GONE);
        startCountDown();

        speechOrbView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!speechOrbView.isPlaying()) {
                    speechOrbView.play();
                    action_prompt.setVisibility(View.VISIBLE);
                    header_prompt.setText("Now Tap on  the Orb to Stop Recording");
                } else {
                    speechOrbView.stop();
                    action_prompt.setVisibility(View.GONE);
                    header_prompt.setText("Tap on  the Orb to Start Recording");
                }
            }
        });

        action_prompt.setVisibility(View.GONE);

        speechOrbView.setListener(new SpeechOrbView.Listener() {
            @Override
            public void onStartPlaying() {

                Toast.makeText(RecordActivity.this, "Recording Started", Toast.LENGTH_LONG)
                        .show();
                stopWatch.start();
            }

            @Override
            public void onEndPlaying() {
                // do something
                Toast.makeText(RecordActivity.this, "Recording topped", Toast.LENGTH_LONG)
                        .show();

                stopAudioRecording();
                audioPlayer.stop();
                stopWatch.stop();
            }
        });

        countdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayer.play(RecordActivity.this, R.raw.countdown);
            }
        });

        crowd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayer.play(RecordActivity.this, R.raw.crowd);
            }
        });

        ghost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayer.play(RecordActivity.this, R.raw.ghost);
            }
        });

        yabba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayer.play(RecordActivity.this, R.raw.yabba_dabba);
            }
        });

        laughing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayer.play(RecordActivity.this, R.raw.laughing);
            }
        });

        zombie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayer.play(RecordActivity.this, R.raw.zombie);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_record, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_restart) {
//            //stopAudioRecording();
//            audioPlayer.stop();
//            recreate();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    public void startCountDown() {
        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                speechOrbView.setVisibility(View.GONE);
                speech_layout.setBackgroundColor(Color.parseColor("#FFF8DC"));
                String sec = String.valueOf(millisUntilFinished / 1000);
                Log.e("Timer", millisUntilFinished + " ");

                if (millisUntilFinished < 1800)
                    countdown_txt.setText("0");
                else
                    countdown_txt.setText(sec);
            }

            public void onFinish() {
                speechOrbView.setVisibility(View.VISIBLE);
                countdown_txt.setVisibility(View.GONE);
                speech_layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_background));

                startAudioRecording();
                speechOrbView.play();

                stopWatch.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                    @Override
                    public void onChronometerTick(Chronometer arg0) {
                        countUp = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;
                        Log.e("count up", countUp+" ");
                        countUp = countUp - 5;
                        String asText = (countUp / 60) + ":" + (countUp % 60);
                        counter_text.setText(asText);
                    }
                });
            }
        }.start();
    }

    private String generateVoiceFilename(int len) {
        //return Utilities.getVerifiedNormalizedPhoneNumber(RecordActivity.this);
        //return "voice";
        return Utilities.getVerifiedNormalizedPhoneNumber(RecordActivity.this);
    }

    private void startAudioRecording() {
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopAudioRecording() {
        if (mediaRecorder != null) {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
        }
        File file = new File(voiceStoragePath);
        mFileUri = Uri.fromFile(file);

        nextActivity();
    }

    private void initializeMediaRecord() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(voiceStoragePath);
    }

    private void hasSDCard() {
        Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        if (isSDPresent) {
            System.out.println("There is SDCard");
        } else {
            System.out.println("There is no SDCard");
        }
    }

    public void nextActivity() {
        Intent i = new Intent(RecordActivity.this, RecordFinalActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(RecordActivity.this, ProfileActivity.class);
        startActivity(i);
        finish();
    }
}
