package com.bedees.android.ui;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bedees.android.R;
import com.bedees.android.adapter.CricketMatchAdapter;
import com.bedees.android.application.Application;
import com.bedees.android.item.CricketItem;
import com.bedees.android.util.Api;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mk on 4/24/2016.
 */
public class CricketMatchFragment extends Fragment implements View.OnClickListener{

    private boolean check_backbtn = false;
    private View view;
    private EditText txt_search;
    private ListView listview;
    private ProgressDialog progressDialog;
    private ArrayList<CricketItem> cricket_list;
    private ArrayList<CricketItem> list_search;

    /*public static CricketMatchFragment newInstance() {
        CricketMatchFragment fragment = new CricketMatchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cricketmatch, container, false);
        ConnectivityManager cn=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf=cn.getActiveNetworkInfo();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        txt_search = (EditText) view.findViewById(R.id.txt_search);
        listview = (ListView) view.findViewById(R.id.listview);
        if(nf != null && nf.isConnected()==true )
        {
        MatchApi();
        }
        else
        {
            Toast.makeText(getActivity(), "Network Not Available", Toast.LENGTH_SHORT).show();
        }

        txt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!txt_search.getText().toString().equals("")) {
                    check_backbtn = false;
                    Search(txt_search.getText().toString());
                } else {
                    try {
                        check_backbtn = true;
                        CricketMatchAdapter cricketMatchAdapter = new CricketMatchAdapter(getActivity(), cricket_list, CricketMatchFragment.this);
                        listview.setAdapter(cricketMatchAdapter);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });




        ((ContactActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ContactActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        ContactActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().popBackStack();
            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {

        }
    }



    public void MatchApi() {

        progressDialog.show();
        RequestQueue queue = Application.getInstance().getRequestQueue();

        Log.e("cricket_url", Api.CRICKET + "");

        StringRequest myReq = new StringRequest(Request.Method.GET, Api.CRICKET,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Cricket Resoponse", response + "..");

                        try {

                            JSONObject json = new JSONObject(response);

                            JSONArray jsonArray = json.getJSONArray("data");

                            cricket_list = new ArrayList<>();
                            for (int i=0; i<jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);
                                CricketItem cricketItem = new CricketItem();

                                cricketItem.setUnique_id(object.getString("unique_id"));
                                cricketItem.setDescription(object.getString("description"));
                                cricketItem.setTitle(object.getString("title"));

                                cricket_list.add(cricketItem);
                            }

                            CricketMatchAdapter cricketMatchAdapter = new CricketMatchAdapter(getActivity(),cricket_list, CricketMatchFragment.this);
                            listview.setAdapter(cricketMatchAdapter);
                            progressDialog.dismiss();

                        } catch (Exception e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {

                Log.e("Resoponse errror", arg0 + "..");
                Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        queue.add(myReq);


    }


    public void MatchScoreApi(final String match_id) {

        progressDialog.show();
        RequestQueue queue = Application.getInstance().getRequestQueue();

        Log.e("Score_url", Api.CRICKET_SCORE+match_id + "");

        StringRequest myReq = new StringRequest(Request.Method.GET, Api.CRICKET_SCORE+match_id,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Score Resoponse", response + "..");

                        try {

                            JSONObject json = new JSONObject(response);

                            String score = json.getString("score");
                            Log.e("score", score);
                            reciveCustomNotification(score, Integer.parseInt(match_id));
                            Toast.makeText(getActivity(),"Please check score notification", Toast.LENGTH_SHORT).show();

                            progressDialog.dismiss();

                        } catch (Exception e) {
                            e.printStackTrace();

                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {

                Log.e("Resoponse errror", arg0 + "..");
                Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        queue.add(myReq);


    }






/*============================  Notification Code Start Here ==================================*/



    public void reciveCustomNotification(String score, int noti_id) {


       /* int icon = R.mipmap.ic_launcher;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, "Mizz Score", when);

        NotificationManager mNotificationManager = (NotificationManager) getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);

        RemoteViews contentView = new RemoteViews(getActivity().getPackageName(), R.layout.custom_notification);
        contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
        contentView.setTextViewText(R.id.title, "Mizz Score");
        contentView.setTextViewText(R.id.text, score);
        contentView.setTextViewText(R.id.text2, score);
        notification.contentView = contentView;

        Intent notificationIntent = new Intent(getActivity(), MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, notificationIntent, 0);
        notification.contentIntent = contentIntent;

        notification.flags |= Notification.FLAG_AUTO_CANCEL; //Do not clear the notification
        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
        notification.defaults |= Notification.DEFAULT_SOUND; // Sound

        mNotificationManager.notify(noti_id, notification);*/


        NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(getActivity()).setAutoCancel(true);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String[] events = new String[score.length()];
// Sets a title for the Inbox in expanded layout
        inboxStyle.setBigContentTitle("Mizz Score");
// Moves events into the expanded layout
        for (int i=0; i < events.length; i++) {

            inboxStyle.addLine("Hello");
            inboxStyle.addLine("Hello1323");
            inboxStyle.addLine("Hello252");
        }
// Moves the expanded layout object into the notification object.
        mBuilder1.setStyle(inboxStyle);



        Bitmap icon1 = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity()).setAutoCancel(true)
                .setContentTitle("Mizz Score")
                .setSmallIcon(getNotificationIcon())
                .setLargeIcon(icon1)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(score))
                .setGroup(score);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.setBigContentTitle("Mizz Score");
        bigText.setBuilder(mBuilder1);
        mBuilder.setStyle(bigText);






       /* NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Event tracker")
                .setContentText("Events received");
        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        String[] events = new String[6];
// Sets a title for the Inbox in expanded layout
        inboxStyle.setBigContentTitle("Event tracker details:");
// Moves events into the expanded layout
        for (int i=0; i < events.length; i++) {

            inboxStyle.addLine("hello");
            inboxStyle.addLine("hello");
            inboxStyle.addLine("hello");
            inboxStyle.addLine("hello");
        }
// Moves the expanded layout object into the notification object.
        mBuilder.setStyle(inboxStyle);*/




        Intent resultIntent = new Intent(getActivity(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);


        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);

        NotificationManager mNotificationManager = (NotificationManager) getActivity()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(noti_id, mBuilder.build());


    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }



    public void Search(String keyword) {

        list_search = new ArrayList<>();

        Log.e("list_search keyword", keyword + "...");
        for (int i = 0; i < cricket_list.size(); i++) {
            if (cricket_list.get(i).getTitle().toLowerCase().contains(keyword.toLowerCase())) {

                list_search.add(cricket_list.get(i));
            }
        }

        Log.e("list_search size", list_search.size() + "...");
        CricketMatchAdapter cricketMatchAdapter = new CricketMatchAdapter(getActivity(),list_search,this);
        listview.setAdapter(cricketMatchAdapter);

    }


/*    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Log.e("check_backbtn", check_backbtn + "");
        if (check_backbtn) {
            super.onBackPressed();
            finish();
        }

        else {
            edtSearch.setText("");
            check_backbtn = true;
            gridView.setAdapter(new SellWardrobeAdapter(
                    SellWardrobeActivity.this, list));
            gridView.setSelection(GridViewPosition);

        }
    }*/


}