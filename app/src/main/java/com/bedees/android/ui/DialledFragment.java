package com.bedees.android.ui;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bedees.android.R;
import com.bedees.android.adapter.DialedContactsAdapter;
import com.bedees.android.listeners.CommonClickListener;
import com.bedees.android.listeners.OnProfilePopupListener;
import com.bedees.android.model.Contact;
import com.bedees.android.sqlite.Contract;
import com.bedees.android.sqlite.DBHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class DialledFragment extends Fragment implements OnProfilePopupListener {
    public static final String TAG = DialledFragment.class.getSimpleName();
    private RecyclerView mRecylerRecyclerView;
    private DialedContactsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Contact> contactList ;


    private CommonClickListener commonClickListener = new CommonClickListener() {
        @Override
        public void onCommonClick(View view, int position) {
            Contact contact;
            contact = contactList.get(position);

            if(contact!=null){
                openChatForContact(contact);
            }else{
                Log.e(TAG, "Strange ...Clicked Contact was empty");
            }
        }
    };

    private void openChatForContact(final Contact contact){
        DBHelper helper = new DBHelper(getActivity());
        helper.callDialed(contact.getPhoneNumber(),contact.getName(),contact.getCountryCode());

        //Send Push

    }


    public DialledFragment() {



        Log.e("DialledFragment", "DialledFragment");
        // Required empty public constructor
    }


    public static DialledFragment newInstance() {

        Log.e("newInstance", "newInstance");
        DialledFragment fragment = new DialledFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        contactList = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_dialed, container, false);
        mRecylerRecyclerView = (RecyclerView)view.findViewById(R.id.rv_contacts);
        mRecylerRecyclerView .setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecylerRecyclerView .setLayoutManager(mLayoutManager);

        fillData();


        return view;
    }

    private void fillFromSQLite(){
        DBHelper helper = new DBHelper(getContext());
        try{
            Cursor dialledCursor = null;
            SQLiteDatabase database = helper.getReadableDatabase();
            String projection[] = new String[]{Contract.Log.COLUMN_NAME_NAME,Contract.Log.COLUMN_NAME_NUMBER,Contract.Log.COLUMN_NAME_TIME};
            dialledCursor = database.query(true, Contract.Log.TABLE_NAME,projection, Contract.Log.COLUMN_NAME_DIRECTION +" = ?" ,new String[]{Contract.Log.OUTGOING},null,null, Contract.Log.COLUMN_NAME_TIME+" DESC","50");
            contactList.clear();
            while(dialledCursor.moveToNext()){
                Contact  contact = new Contact();
                contact.setName(dialledCursor.getString(0));
                contact.setPhoneNumber(dialledCursor.getString(1));
                contact.setLastContacted(dialledCursor.getInt(2));
                contactList.add(contact);

            }

            dialledCursor.close();
        }catch (Exception exp){
            Log.e(TAG,"Exception while loading from database "+exp);
        }
    }
    private void fillData(){

        fillFromSQLite();
        mAdapter = new DialedContactsAdapter(contactList,commonClickListener);
        mAdapter.setOnProfilePopupListener(this);
        mRecylerRecyclerView .setAdapter(mAdapter);

    }

    public void reFreshDialedList(){

        FragmentTransaction trans = getFragmentManager().beginTransaction();
        trans.replace(R.id.container, new DialledFragment());
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack(null);
        trans.commit();
    }


    @Override
    public void onResume() {
        super.onResume();
        fillData();
    }

    /**
     * Add the items int the contactList.
     */
    private void fillContactList(){
        //TODO: fill Contact List from database
        Contact contact;
        contactList = new ArrayList<>();
    /*    contact = new Contact();
        contact.setContactId("11");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Ran Chandi");
        contact.setLastContacted(System.currentTimeMillis() - 20000);
        contactList.add(contact);
*/
        contact = new Contact();
        contact.setContactId("10");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Radhika Gauri");
        contact.setLastContacted(System.currentTimeMillis() - 30000);
        contactList.add(contact);

        contact = new Contact();
        contact.setContactId("9");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Krishna Chandra");
        contact.setLastContacted(System.currentTimeMillis() - 40000);
        contactList.add(contact);


        contact = new Contact();
        contact.setContactId("8");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Anniruddha Acharya ");
        contact.setLastContacted(System.currentTimeMillis() - 500000);
        contactList.add(contact);


  /*      contact = new Contact();
        contact.setContactId("7");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Devkinandan");
        contact.setLastContacted(System.currentTimeMillis() - 600000);
        contactList.add(contact);
*/

        contact = new Contact();
        contact.setContactId("6");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Gaurav Krishna");
        contact.setLastContacted(System.currentTimeMillis() - 700000);
        contactList.add(contact);


        contact = new Contact();
        contact.setContactId("5");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("AB DeVilliers");
        contact.setLastContacted(System.currentTimeMillis() - 800000);
        contactList.add(contact);

        contact = new Contact();
        contact.setContactId("4");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Michael Schumacher");
        contact.setLastContacted(System.currentTimeMillis() - 9000000);
        contactList.add(contact);

        contact = new Contact();
        contact.setContactId("3");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Ronaldo");
        contact.setLastContacted(System.currentTimeMillis() - 9700000);
        contactList.add(contact);


/*        contact = new Contact();
        contact.setContactId("2");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Sachin Tendulkar");
        contact.setLastContacted(System.currentTimeMillis() - 10700000);
        contactList.add(contact);

        contact = new Contact();
        contact.setContactId("1");
        contact.setImageUrl("http://image.com/image.png");
        contact.setName("Rafael Nadal");
        contact.setLastContacted(System.currentTimeMillis() - 11700000);
        contactList.add(contact);
  */      sortContactsByTime();
    }

    private void sortContactsByTime(){
        Collections.sort(contactList, new Comparator<Contact>() {
            @Override
            public int compare(Contact lhs, Contact rhs) {
                return -((Long) lhs.getLastContacted()).compareTo((Long) rhs.getLastContacted());
            }
        });
    }

    @Override
    public void onShowProfilePopup(Contact contact) {
        ProfilePopupDialog.newInstance(contact).show(getActivity().getSupportFragmentManager(),
                ProfilePopupDialog.class.getSimpleName());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAdapter.setOnProfilePopupListener(null);
    }


  /*  @Override
    public void onDetach() {
        super.onDetach();
        Toast.makeText(getActivity(), "onDetach", Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();
        Toast.makeText(getActivity(), "onDestroyOptionsMenu", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Toast.makeText(getActivity(), "onDestroyView", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Toast.makeText(getActivity(), "onViewStateRestored", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Toast.makeText(getActivity(), "onAttach", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Toast.makeText(getActivity(), "onAttach_s", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Toast.makeText(getActivity(), "onActivityCreated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(getActivity(), "onActivityResult", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {

        Toast.makeText(getActivity(), "onCreateAnimation", Toast.LENGTH_SHORT).show();
        return super.onCreateAnimation(transit, enter, nextAnim);

    }

    @Override
    public void onStart() {
        super.onStart();
        Toast.makeText(getActivity(), "onViewCreated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toast.makeText(getActivity(), "onViewCreated", Toast.LENGTH_SHORT).show();
    }*/
}
