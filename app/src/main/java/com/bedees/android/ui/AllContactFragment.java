package com.bedees.android.ui;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.adapter.ContactsAdapter;
import com.bedees.android.listeners.CommonClickListener;
import com.bedees.android.listeners.OnProfilePopupListener;
import com.bedees.android.model.Contact;
import com.bedees.android.sqlite.DBHelper;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.SavePref;
import com.bedees.android.util.Utilities;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


public class AllContactFragment extends Fragment implements OnProfilePopupListener {
    public static final String TAG = AllContactFragment.class.getSimpleName();
    private RecyclerView mRecylerRecyclerView;
    private ContactsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Contact> contactList;
    //ArrayList<Contact> contactList_random = new ArrayList<>();
    private ArrayList<String> contactList_name = new ArrayList<>();
    public static ArrayList<HashMap<String, String>> dialed_list = new ArrayList<>();

    private SavePref pref;
    private boolean sendPush = true;
    private int time_counter = 10;
    private Timer timer;
    private String calling_number = "123456789";
    private Random randomGenerator;
    private Firebase mFireBase;
    private String newNumber_;

    private CommonClickListener commonClickListener = new CommonClickListener() {
        @Override
        public void onCommonClick(View view, int position) {
            try {
                Contact contact;
                sendPush = true;
                contact = contactList.get(position);

                if (contact != null) {
                    if (sendPush || !calling_number.equals(contact.getPhoneNumber())) {

                        calling_number = contact.getPhoneNumber();
                        //  String perviousnumber=
                        // if(calling_number.equalsIgnoreCase(perviousnumber)){
                        sendPush = false;
                        new Thread(new Task()).start();
                        // }

                        sendPushCall(contact);
                    }
                } else {
                    Log.e(TAG, "Strange ...Clicked Contact was empty");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void sendPushCall(final Contact contact) {

        pref.setCaller_name(contact.getName());

        DBHelper helper = new DBHelper(getActivity());
        helper.callDialed(contact.getPhoneNumber(), contact.getName(), contact.getCountryCode());

        //Send Push
        String sender = Utilities.getVerifiedNormalizedPhoneNumber(getContext());

        Log.e(TAG, contact.getNormalizedNumber() + " is called by " + sender);
        mFireBase.child("call").child(contact.getNormalizedNumber()).setValue(sender);
    }

    public AllContactFragment() {
    }


    public static AllContactFragment newInstance() {
        AllContactFragment fragment = new AllContactFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_contact, container, false);

        pref = new SavePref(getActivity());
        contactList = new ArrayList<>();
        mRecylerRecyclerView = (RecyclerView) view.findViewById(R.id.rv_contacts);

        mRecylerRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecylerRecyclerView.setLayoutManager(mLayoutManager);

        fillData();

        mFireBase = new Firebase(FirebaseConstants.FIRE_BASE_URL);

        mRecylerRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecylerRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Contact contact = contactList.get(position);
                Toast.makeText(getActivity(), contact.getNormalizedNumber(), Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getActivity(), UserProfileActivity.class);
                i.putExtra("phone_number", contact.getNormalizedNumber());
                i.putExtra("name", contact.getName());
                i.putExtra("image_url", contact.getImageUrl());
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        addPhoneAddListener();
        return view;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private void stubContactList() {
        Contact contact = new Contact();
        contact.setContactId("12");
        contact.setCountryCode("096");
        contact.setName("Vitaliy");
        contact.setPhoneNumber("0967812934");
        contactList.add(contact);
    }

    @Override
    public void onShowProfilePopup(Contact contact) {
        ProfilePopupDialog.newInstance(contact).show(getActivity().getSupportFragmentManager(),
                ProfilePopupDialog.class.getSimpleName());
    }

    class Task implements Runnable {

        @Override
        public void run() {

            try {
                Thread.sleep(11000);
                sendPush = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //addPhoneAddListener();

        contactList = new ArrayList<>();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    private void addPhoneAddListener() {
        mFireBase.child("phone").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG, "Child Added at" + dataSnapshot.getRef() + " with key " + dataSnapshot.getKey() + "  with value " + dataSnapshot.getValue() + " type " + dataSnapshot.getClass());
                onNewInstallationAdded(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    private void onNewInstallationAdded(String newNumber) {
        newNumber_ = newNumber;
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    String arr[] = newNumber_.split("-");
                    String newCountryCode = arr[0];
                    String newPhoneNumber = arr[1];

                    if (!pref.getPhoneNumber().equals(newPhoneNumber)) {

                        String[] projection = new String[]{
                                ContactsContract.PhoneLookup.DISPLAY_NAME,
                                ContactsContract.PhoneLookup._ID,
                                ContactsContract.PhoneLookup.NORMALIZED_NUMBER

                        };
                        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(newPhoneNumber));
                        ContentResolver cr = getActivity().getContentResolver();
                        Cursor crc = cr.query(contactUri, projection, null, null, null);
                        String name, id, number;


                        if (crc.moveToNext()) {
                            name = crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                            id = crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup._ID));
                            number = crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.NORMALIZED_NUMBER));

                            Contact contact = new Contact();
                            contact.setContactId(id);
                            contact.setNormalizedNumber(newCountryCode + "-" + newPhoneNumber);
                            contact.setCountryCode(newCountryCode);
                            contact.setName(name);
                            contact.setPhoneNumber(number);

                            contactList.add(contact);
                            Log.e("name", name);

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Log.d("UI thread", "I am the UI thread");
                                    fillData();
                                    mAdapter.notifyDataSetChanged();
                                }
                            });


                        } else {
                            name = "Unknown";
                            Log.e(TAG, "New Installation not known");
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }

    private void fillData() {

        Set<Contact> hs = new HashSet<>();
        hs.addAll(contactList);
        contactList.clear();
        contactList.addAll(hs);

        mAdapter = new ContactsAdapter(contactList, commonClickListener);
        mRecylerRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnProfilePopupListener(this);
        mAdapter.notifyDataSetChanged();
    }


    public void TimerDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_remarks);
        dialog.setCanceledOnTouchOutside(true);

        TextView lb_submit = (TextView) dialog.findViewById(R.id.lb_submit);
        final TextView txt_remark = (TextView) dialog.findViewById(R.id.txt_remark);

        try {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Log.e("Thread", "Thread call");

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // update TextView here!
                            time_counter--;
                            if (time_counter >= 1) {
                                txt_remark.setText("Please wait for " + String.valueOf(time_counter) + " sec");
                            } else {
                                timer.cancel();
                                dialog.dismiss();
                            }
                        }
                    });


                }
            }, 0, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        lb_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
