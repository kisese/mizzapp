package com.bedees.android.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.Utilities;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

import co.mobiwise.library.InteractivePlayerView;

public class RecordSaveActivity extends AppCompatActivity {

    private ImageButton mIbpause;
    private ImageButton mIbplay;
    private ImageButton mIbrecord;
    private InteractivePlayerView playerView;
    private String voiceStoragePath;
    private int totalDuration = 123;
    private MediaPlayer mediaPlayer;
    private CountDownTimer timer;
    private int progress;
    private Button save, record_again, share;
    private FirebaseAuth mAuth;
    private Firebase mFireBase;
    private Firebase mFirebaseRef;
    private StorageReference storageRef;
    private ProgressDialog mProgressDialog;
    private Uri mDownloadUrl = null;
    private Uri mFileUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_record_save);

        mFireBase = new Firebase(FirebaseConstants.FIRE_BASE_URL);

        mIbplay = (ImageButton) findViewById(R.id.ib_play);
        mIbpause = (ImageButton) findViewById(R.id.ib_pause);
        playerView = (InteractivePlayerView) findViewById(R.id.seek_audio_status);
        save = (Button) findViewById(R.id.btn_save);
        record_again = (Button) findViewById(R.id.btn_record_again);
        share = (Button) findViewById(R.id.btn_share);

        voiceStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File audioVoice = new File(voiceStoragePath + File.separator + "voices");
        if (!audioVoice.exists()) {
            audioVoice.mkdir();
        }
        voiceStoragePath = voiceStoragePath + File.separator + "voices/final" + generateVoiceFilename(6) + ".mp3";

        playerView.setMax(totalDuration);

        mFirebaseRef = new Firebase(FirebaseConstants.FIRE_BASE_URL);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        //get firebase storage instance
        FirebaseStorage storage = FirebaseStorage.getInstance();

        // Create a storage reference from our app
        storageRef = storage.getReferenceFromUrl("gs://project-1958300502970119287.appspot.com");

        // Create a reference to "mountains.jpg"
        StorageReference mountainsRef = storageRef.child(voiceStoragePath);

        // Create a reference to 'images/mountains.jpg'
        StorageReference mountainImagesRef = storageRef.child("voice/final" + voiceStoragePath);

        // While the file names are the same, the references point to different files
        mountainsRef.getName().equals(mountainImagesRef.getName());
        mountainsRef.getPath().equals(mountainImagesRef.getPath());
        System.out.println("mountainsRef.getName(): " + mountainsRef.getName().equals(mountainImagesRef.getName()));

        signInAnonymously();

        try {
            Uri uri = Uri.parse(voiceStoragePath);
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(RecordSaveActivity.this, uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int millSecond = Integer.parseInt(durationStr);

            totalDuration = millSecond / 1000;
            Log.e("Total Duration", totalDuration + " ");

            playerView.setMax(totalDuration);

        } catch (Exception e) {
            e.printStackTrace();
            mIbpause.setEnabled(false);
            mIbplay.setEnabled(false);
        }

        mIbplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RecordSaveActivity.this, "Play", Toast.LENGTH_SHORT).show();
                playLastStoredAudioMusic();
                mediaPlayerPlaying();
                startCountDown(totalDuration);
                playerView.start();
            }
        });

        mIbpause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RecordSaveActivity.this, "Pause", Toast.LENGTH_SHORT).show();
                //stopAudioRecording();
                stopAudioPlay();
                playerView.stop();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = new File(voiceStoragePath);
                mFileUri = Uri.fromFile(file);
                uploadFromUri(mFileUri);

                Intent i = new Intent(RecordSaveActivity.this, ContactActivity.class);
                startActivity(i);
                finish();
            }
        });

        record_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RecordSaveActivity.this, ProfileActivity.class);
                startActivity(i);
                finish();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(voiceStoragePath);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("audio/*");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(share, "Share Sound File"));
            }
        });
    }

    private String generateVoiceFilename(int len) {
        return Utilities.getVerifiedNormalizedPhoneNumber(RecordSaveActivity.this);
    }


    private void playLastStoredAudioMusic() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(voiceStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
//        mIbrecord.setEnabled(true);
//        mIbplay.setEnabled(false);
    }

    private void stopAudioPlay() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    private void hasSDCard() {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (isSDPresent) {
            System.out.println("There is SDCard");
        } else {
            System.out.println("There is no SDCard");
        }
    }

    private void mediaPlayerPlaying() {
        if (!mediaPlayer.isPlaying()) {
            stopAudioPlay();
        }
    }

    public void startCountDown(final long totalLength) {
        //startAudioRecording();
        timer = new CountDownTimer((totalLength * 1000), 1000) {

            public void onTick(long millisUntilFinished) {
                Log.e("mill until finished", millisUntilFinished + "  - " + (int) (millisUntilFinished / 1000));
                progress = (int) (millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.e("Stopped playing", "length " + totalLength + " ");
                playerView.stop();
                playerView.setProgress(0);
            }


        }.start();
    }

    // [START upload_from_uri]
    private void uploadFromUri(Uri fileUri) {
        Log.d("DEBUG", "uploadFromUri:src:" + fileUri.toString());

        // [START get_child_ref]
        // Get a reference to store file at photos/<FILENAME>.jpg
        final StorageReference photoRef = storageRef.child("voices")
                .child(fileUri.getLastPathSegment());
        // [END get_child_ref]

        // Upload file to Firebase Storage
        // [START_EXCLUDE]
        showProgressDialog();
        // [END_EXCLUDE]
        Log.d("DEBUG", "uploadFromUri:dst:" + photoRef.getPath());
        photoRef.putFile(fileUri)
                .addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Upload succeeded
                        Log.d("DEBUG", "uploadFromUri:onSuccess");

                        // Get the public download URL
                        mDownloadUrl = taskSnapshot.getMetadata().getDownloadUrl();

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        //updateUI(mAuth.getCurrentUser());
                        // [END_EXCLUDE]
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Upload failed
                        Log.w("DEBUG", "uploadFromUri:onFailure", exception);

                        mDownloadUrl = null;

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        Toast.makeText(RecordSaveActivity.this, "Error: upload failed",
                                Toast.LENGTH_SHORT).show();
                        //updateUI(mAuth.getCurrentUser());
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END upload_from_uri]

    private void signInAnonymously() {
        // Sign in anonymously. Authentication is required to read or write from Firebase Storage.
        showProgressDialog();
        mAuth.signInAnonymously()
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Log.d("DEBUG", "signInAnonymously:SUCCESS");
                        hideProgressDialog();
                        Log.e("AUTH RESULT", authResult.getUser().toString());
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e("AUTH RESULT", "signInAnonymously:FAILURE", exception);
                        hideProgressDialog();
                        //updateUI(null);
                    }
                });
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Saving...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(RecordSaveActivity.this, ProfileActivity.class);
        startActivity(i);
        finish();
    }
}
