package com.bedees.android.ui;

import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.manager.FirebaseManager;
import com.bedees.android.model.ProfilePicture;
import com.bedees.android.util.DownloadStatus;
import com.bumptech.glide.Glide;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.File;

import co.mobiwise.library.InteractivePlayerView;

public class UserProfileActivity extends AppCompatActivity {

    private String voiceStoragePath;
    private Uri mFileUri;
    private DownloadStatus downloadStatus;
    private MediaPlayer mediaPlayer;
    private ImageView profileImage;
    private TextView status;
    private String name, image_url, phone_number;
    private InteractivePlayerView player;
    private int totalDuration = 123;
    private ImageButton ib_play, ib_stop;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        final ActionBar actionBar = ((UserProfileActivity)this).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.hide();
        }

        Intent i = getIntent();
        phone_number = i.getStringExtra("phone_number");
        name = i.getStringExtra("name");
        image_url = i.getStringExtra("image_url");

        Log.e("Details", "phone Number  " + phone_number + " name " + name + "image url " + image_url);

        profileImage = (ImageView) findViewById(R.id.iv_profile_picture);
        player = (InteractivePlayerView) findViewById(R.id.player);
        ib_play = (ImageButton) findViewById(R.id.ib_play);
        ib_stop = (ImageButton) findViewById(R.id.ib_pause);
        downloadStatus = new DownloadStatus(UserProfileActivity.this);
        voiceStoragePath = voiceStoragePath + File.separator + "voices/final" + phone_number + ".mp3";
        file = new File(voiceStoragePath);
        mFileUri = Uri.fromFile(file);

        player.setMax(totalDuration);

        FirebaseManager.getInstance().getProfilePictureForPhoneNumber(phone_number, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Log.e("Picture", "Loading picture");
                    ProfilePicture picture = dataSnapshot.getValue(ProfilePicture.class);
                    if (picture != null) {
                        picture.getUrl();
                        showProfilePicture(picture.getUrl());
                        Log.e("Picture", "Loading picture " +picture.getUrl() + " " + picture.getPhone_no());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                profileImage.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
            }
        });

        if (downloadStatus.getDownloadStatus()==false) {
            beginDownload();
        } else {
            //begin play
            getTotalDuration();
        }

        ib_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (downloadStatus.getDownloadStatus()) {
                    if (!file.exists()) {
                        if(downloadStatus.getDownloadStatus()) {
                            getTotalDuration();
                            startAudioPlay();
                            player.stop();
                        }
                    } else {
                        Toast.makeText(UserProfileActivity.this, "Downloading voice please wait", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        ib_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (downloadStatus.getDownloadStatus()) {
                    if (!file.exists()) {
                        stopAudioPlay();
                        player.stop();
                        player.setProgress(0);
                    } else {
                        Toast.makeText(UserProfileActivity.this, "Downloading voice please wait", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        Log.e("phone number", phone_number);
    }

    private void beginDownload() {
        // Get path
        String path = "voices/" + mFileUri.getLastPathSegment();

        // Kick off download service
        Intent intent = new Intent(UserProfileActivity.this, MyDownloadService.class);
        intent.setAction(MyDownloadService.ACTION_DOWNLOAD);
        intent.putExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH, path);
        UserProfileActivity.this.startService(intent);

        Toast.makeText(UserProfileActivity.this, "Downloading voice please wait", Toast.LENGTH_LONG).show();
    }

    /**
     * Displays Contact picture in ImageView via Glide
     */
    private void showProfilePicture(String url) {
        if (!TextUtils.isEmpty(url)) {
            player.setCoverURL(url);
            Glide.with(UserProfileActivity.this)
                    .load(url)
                    .placeholder(getResources().getDrawable(R.drawable.placeholder))
                    .into(profileImage);
        }
    }

    public void startAudioPlay() {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(voiceStoragePath);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {

        }
    }

    private void stopAudioPlay() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void getTotalDuration() {
        try {
            Uri uri = Uri.parse(voiceStoragePath);
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(UserProfileActivity.this, uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int millSecond = Integer.parseInt(durationStr);

            totalDuration = millSecond / 1000;
            Log.e("Total Duration", totalDuration + " ");

            player.setMax(totalDuration);
        } catch (Exception e) {
            Toast.makeText(UserProfileActivity.this, "Downloading voice please wait", Toast.LENGTH_LONG).show();
            beginDownload();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(UserProfileActivity.this, ContactActivity.class);
        startActivity(i);
    }
}
