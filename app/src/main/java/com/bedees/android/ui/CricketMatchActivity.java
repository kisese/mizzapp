package com.bedees.android.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bedees.android.R;
import com.bedees.android.application.Application;
import com.bedees.android.item.CricketItem;
import com.bedees.android.util.Api;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CricketMatchActivity extends Activity implements View.OnClickListener{

    private EditText txt_search;
    private ListView listview;
    private ProgressDialog progressDialog;
    private ArrayList<CricketItem> cricket_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cricket_match);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        txt_search = (EditText) findViewById(R.id.txt_search);
        listview = (ListView) findViewById(R.id.listview);

        MatchApi();
    }



@Override
public void onClick(View v) {

        switch (v.getId())
        {

        }
}



   public void MatchApi() {


        progressDialog.show();
        RequestQueue queue = Application.getInstance().getRequestQueue();

        Log.e("cricket_url", Api.CRICKET + "");

        StringRequest myReq = new StringRequest(Request.Method.GET, Api.CRICKET,
        new Response.Listener<String>() {

   @Override
   public void onResponse(String response) {
        Log.e("Cricket Resoponse", response + "..");

        try {

        JSONObject json = new JSONObject(response);

        JSONArray jsonArray = json.getJSONArray("data");

        cricket_list = new ArrayList<>();
        for (int i=0; i<jsonArray.length(); i++) {

        JSONObject object = jsonArray.getJSONObject(i);
        CricketItem cricketItem = new CricketItem();

        cricketItem.setUnique_id(object.getString("unique_id"));
        cricketItem.setDescription(object.getString("description"));
        cricketItem.setTitle(object.getString("title"));

        cricket_list.add(cricketItem);
        }

        /*CricketMatchAdapter cricketMatchAdapter = new CricketMatchAdapter(CricketMatchActivity.this,cricket_list);
        listview.setAdapter(cricketMatchAdapter);*/
        progressDialog.dismiss();

        } catch (Exception e) {
        e.printStackTrace();

        progressDialog.dismiss();
        Toast.makeText(CricketMatchActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }



        }
        }, new Response.ErrorListener() {

    @Override
    public void onErrorResponse(VolleyError arg0) {

        Log.e("Resoponse errror", arg0 + "..");
        Toast.makeText(CricketMatchActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
        }
        });
        queue.add(myReq);


        }

        }
