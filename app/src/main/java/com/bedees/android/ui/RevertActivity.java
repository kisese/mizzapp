package com.bedees.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bedees.android.R;
import com.bedees.android.util.Utilities;

import org.w3c.dom.Text;

public class RevertActivity extends AppCompatActivity {
    public static final String TAG = RevertActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String message = intent.getStringExtra(ReceiveActivity.KEY_MESSAGE);
        String sender = intent.getStringExtra(ReceiveActivity.KEY_SENDER);
        String messageText = message.split(":")[0].equals("thanks") ? getString(R.string.thanks_message) : getString(R.string.wait_message);
        String title = message.equals("thanks") ? "Thanks" : "Wait";
        setTitle(title);
        int index = sender.indexOf("-")+1;
        String name = Utilities.getContactNameFromNumber(this,sender.substring(index));
        Log.e(TAG,"Name : "+name);

        TextView senderTextView = (TextView)findViewById(R.id.tv_sender);
        senderTextView.setText(name);

        TextView messageTextView = (TextView)findViewById(R.id.tv_reply);
        messageTextView.setText(messageText);

    }


    public void onFinishClick(View view){
        finish();
    }

}
