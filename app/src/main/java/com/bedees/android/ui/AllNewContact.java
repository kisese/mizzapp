package com.bedees.android.ui;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bedees.android.R;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.SavePref;
import com.firebase.client.Firebase;

/**
 * Created by dhiraj on 13/5/16.
 */
public class AllNewContact extends Fragment {
    @Nullable

    RecyclerView mRecylerRecyclerView ;
    SavePref pref;
    LinearLayoutManager mLayoutManager;
    Firebase mFireBase;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_contact, container, false);

        pref = new SavePref(getActivity());
        mRecylerRecyclerView = (RecyclerView) view.findViewById(R.id.rv_contacts);

        mRecylerRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecylerRecyclerView.setLayoutManager(mLayoutManager);

        //fillData();

        mFireBase = new Firebase(FirebaseConstants.FIRE_BASE_URL);


        return view;
    }
}
