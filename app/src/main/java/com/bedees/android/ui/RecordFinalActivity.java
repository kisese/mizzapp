package com.bedees.android.ui;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.adapter.BackgroundsListAdapter;
import com.bedees.android.adapter.EffectsListAdapter;
import com.bedees.android.util.AudioPlayer;
import com.bedees.android.util.Utilities;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import bedees.lib.superpoweredfx.SuperpoweredFX;
import co.mobiwise.library.InteractivePlayerView;
import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;

public class RecordFinalActivity extends AppCompatActivity {

    private InteractivePlayerView ipv;
    String voiceStoragePath, voiceRecordingPath;
    MediaPlayer mediaPlayer;
    private int totalDuration = 123;
    private ImageView effect_a, effect_b, effect_c, effect_d, effect_e;
    private HListView effects_listview, backgrounds_listview;
    private EffectsListAdapter effectsListAdapter;
    private BackgroundsListAdapter backgroundsListAdapter;
    private Timer fakeTouchTimer;
    private List<String> effects = new ArrayList<>();
    private List<String> backgrounds = new ArrayList<>();
    private Button show_backgrounds, show_efects;
    private AudioPlayer audioPlayer = new AudioPlayer();
    private int[] background_sounds = {R.raw.silence, R.raw.birthday, R.raw.easy, R.raw.guitar, R.raw.happy, R.raw.instrumental,
            R.raw.lullaby, R.raw.piano, R.raw.rain, R.raw.rhytmic, R.raw.winter, R.raw.deejay, R.raw.cashier, R.raw.temple,
            R.raw.story, R.raw.princess, R.raw.electro};
    private CountDownTimer timer;
    private boolean fx_selected = false, playing = true, backgrounds_selected = false, music_playing = true;
    private int effects_position = 0, backgrounds_position = 0;
    private ImageView play_pause;
    private SuperpoweredFX fx = new SuperpoweredFX();
    private MediaRecorder mediaRecorder;
    private File outputDir;
    private File outputFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_final);

        ipv = (InteractivePlayerView) findViewById(R.id.ipv);
        effects_listview = (HListView) findViewById(R.id.effects_listview);
        backgrounds_listview = (HListView) findViewById(R.id.backgrounds_listview);
        show_backgrounds = (Button) findViewById(R.id.show_backgrounds);
        show_efects = (Button) findViewById(R.id.show_effects);
        play_pause = (ImageView) findViewById(R.id.playPause);

        backgroundSelected();
        hasSDCard();

        effects.add("None");
        effects.add("Flanger");
        effects.add("Roll");
        effects.add("Whoosh");
        effects.add("Echo");
        effects.add("Reverb");
        effects.add("Limiter");
        effects.add("Gate");
        effects.add("Compressor");
        effects.add("Band EQ");

        backgrounds.add("None");
        backgrounds.add("Birthday");
        backgrounds.add("Easy");
        backgrounds.add("Guitar");
        backgrounds.add("Happy");
        backgrounds.add("Instrumental");
        backgrounds.add("Lullaby");
        backgrounds.add("Piano");
        backgrounds.add("Rain");
        backgrounds.add("Rythmic");
        backgrounds.add("Winter");
        backgrounds.add("DeeJay");
        backgrounds.add("Cashier");
        backgrounds.add("Temple");
        backgrounds.add("Story");
        backgrounds.add("Princess");
        backgrounds.add("Electro");

        effectsListAdapter = new EffectsListAdapter(RecordFinalActivity.this, R.layout.effect_item, effects);
        effects_listview.setAdapter(effectsListAdapter);

        backgroundsListAdapter = new BackgroundsListAdapter(RecordFinalActivity.this, R.layout.effect_item, backgrounds);
        backgrounds_listview.setAdapter(backgroundsListAdapter);

        effects_listview.setVisibility(View.GONE);

        voiceStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File audioVoice = new File(voiceStoragePath + File.separator + "voices");
        if (!audioVoice.exists()) {
            audioVoice.mkdir();
        }

        voiceRecordingPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File recordingVoice = new File(voiceRecordingPath + File.separator + "voices");
        if (!recordingVoice.exists()) {
            recordingVoice.mkdir();
        }

        voiceStoragePath = voiceStoragePath + File.separator + "voices/" + generateVoiceFilename(6) + ".mp3";
        voiceRecordingPath = voiceRecordingPath + File.separator + "voices/final" + generateVoiceFilename(6) + ".mp3";

        initializeMediaRecord();

        // Get the device's sample rate and buffer size to enable low-latency Android audio output, if available.
        String samplerateString = null, buffersizeString = null;
        if (Build.VERSION.SDK_INT >= 17) {
            AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            samplerateString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
            buffersizeString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
        }
        if (samplerateString == null) samplerateString = "44100";
        if (buffersizeString == null) buffersizeString = "512";

        // Files under res/raw are not zipped, just copied into the APK. Get the offset and length to know where our files are located.
        AssetFileDescriptor fd0 = getResources().openRawResourceFd(R.raw.century_fox);
        //AssetFileDescriptor fd1 = getResources().openRawResourceFd(R.raw.nuyorica);
        FileDescriptor fd1 = null;
        int length_b = 0;
        try {
//            File baseDir = Environment.getExternalStorageDirectory();
//            voiceStoragePath = baseDir.getAbsolutePath() + "/example" + ".mp3";

            FileInputStream fis = new FileInputStream(voiceStoragePath);
            fd1 = fis.getFD();
            length_b = (int) (new File(voiceStoragePath).length());
            Log.e("Length", length_b + " " + voiceStoragePath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Uri uri = Uri.parse(voiceStoragePath);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(RecordFinalActivity.this, uri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        int millSecond = Integer.parseInt(durationStr);

        totalDuration = millSecond / 1000;
        ipv.setMax(totalDuration);

        //int fileAoffset = (int)fd0.getStartOffset(), fileAlength = (int)fd0.getLength(), fileBoffset = (int)fd1.getStartOffset(), fileBlength = (int)fd1.getLength();

        int fileAoffset = 0, fileAlength = length_b, fileBoffset = 0, fileBlength = length_b;


        try {
            fd0.getParcelFileDescriptor().close();
            //fd1.getParcelFileDescriptor().close();
        } catch (IOException e) {
            Log.d("", "Close error.");
        }

        // Arguments: path to the APK file, offset and length of the two resource files, sample rate, audio buffer size.
        fx.init(Integer.parseInt(samplerateString), Integer.parseInt(buffersizeString), voiceStoragePath, fileAoffset, fileAlength, fileBoffset, fileBlength);

        startPlaying();
        startCountDown(totalDuration + 1);
        startAudioRecording();

        play_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (music_playing) {
                    music_playing = false;
                    play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_playback_play));
                    ipv.stop();
                    ipv.setProgress(0);
                    fx.playPause(false);
                    audioPlayer.stop();
                    stopAudioPlay();
                    stopCountdown();
                } else {
                    restartRecording();
                    music_playing = true;
                    play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_playback_stop));
                    startCountDown(totalDuration + 1);
                    ipv.start();
                    if (backgrounds_selected) {
                        playLastStoredAudioMusic();
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    audioPlayer.stop();
                                    audioPlayer.playAtVolume(RecordFinalActivity.this, background_sounds[backgrounds_position], 80);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();
                    } else if (fx_selected) {
                        fx.fxSelect(effects_position);
                        fx.playPause(playing);
                    } else if (fx_selected && backgrounds_selected) {
                        fx.fxSelect(effects_position);
                        fx.playPause(playing);

                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    audioPlayer.stop();
                                    audioPlayer.playAtVolume(RecordFinalActivity.this, background_sounds[backgrounds_position], 80);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        thread.start();

                    } else {
                        playLastStoredAudioMusic();
                    }
                }
            }
        });

        effects_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fx.fxOff();
                fx.fxSelect(position);
                audioPlayer.stop();
                stopAudioPlay();
                fx.playPause(false);
                fx_selected = true;
                effects_position = position;
                restartRecording();

                if (backgrounds_selected) {
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                audioPlayer.stop();
                                audioPlayer.playAtVolume(RecordFinalActivity.this, background_sounds[backgrounds_position], 80);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    thread.start();
                }

                if (effects.get(position).equalsIgnoreCase("Roll")) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fx.crossFader(100);
                            fx.fxValue(10);
                        }
                    }, 500);
                } else {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fx.crossFader(100);
                            fx.fxValue(100);
                        }
                    }, 500);
                }


                Log.e("Position", position + " ");

                ipv.stop();
                ipv.setProgress(0);
                ipv.start();
                fx.playPause(playing);
                stopCountdown();
                startCountDown(totalDuration + 1);
                Toast toast = Toast.makeText(getApplicationContext(),
                        effects.get(position) + " Selected", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        });

        backgrounds_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ipv.stop();
                ipv.setProgress(0);
                ipv.start();
                audioPlayer.stop();
                stopAudioPlay();
                fx.playPause(false);
                backgrounds_position = position;
                backgrounds_selected = true;
                restartRecording();

                if (fx_selected) {
                    fx.fxOff();
                    fx.fxSelect(effects_position);

                    if (effects.get(effects_position).equalsIgnoreCase("Roll")) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                fx.crossFader(100);
                                fx.fxValue(20);
                            }
                        }, 500);
                    } else {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                fx.crossFader(100);
                                fx.fxValue(100);
                            }
                        }, 500);
                    }

                    Log.e("Position", effects_position + " ");
                    fx.playPause(playing);
                }

                if (backgrounds.get(position).equalsIgnoreCase("None")) {
                    playLastStoredAudioMusic();
                } else {
                    audioPlayer.playAtVolume(RecordFinalActivity.this, background_sounds[position], 80);
                    playLastStoredAudioMusic();
                }
                stopCountdown();
                startCountDown(totalDuration + 1);
            }
        });

        show_backgrounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ipv.stop();
//                ipv.setProgress(0);
                backgroundSelected();
//                onPlayPause(false);
//                audioPlayer.stop();
//                stopAudioPlay();
                backgrounds_listview.setVisibility(View.VISIBLE);
                effects_listview.setVisibility(View.GONE);
            }
        });

        show_efects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ipv.stop();
//                ipv.setProgress(0);
                effectsSelected();
                backgrounds_listview.setVisibility(View.GONE);
                effects_listview.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_record, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_record) {
            //stopAudioRecording();
            stopAudioRecording();
            audioPlayer.stop();
            stopAudioPlay();
            fx.playPause(false);

            try {
//
//                File myFile = new File(voiceStoragePath);
//                myFile.createNewFile();
//
//                byte[] bytes = new byte[(int) myFile.length()];
//                FileOutputStream out = new FileOutputStream(myFile);
//                out.write(bytes);
//                out.close();
//
//                Log.e("File Created: ",  myFile.getAbsolutePath()  + " ");
            }catch (Exception e){
                e.printStackTrace();
            }

            Intent i = new Intent(RecordFinalActivity.this, RecordSaveActivity.class);
            startActivity(i);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String generateVoiceFilename(int len) {
        //return Utilities.getVerifiedNormalizedPhoneNumber(RecordFinalActivity.this);
        return Utilities.getVerifiedNormalizedPhoneNumber(RecordFinalActivity.this);
    }

    private void playLastStoredAudioMusic() {
        mediaPlayer = new MediaPlayer();
        final AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        final int originalVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC), 0);
        //mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
//        final float volume = (float) (1 - (Math.log(100 - 80) / Math.log(100)));
//        mediaPlayer.setVolume(volume, volume);
        //Log.e("Audio Volume ", volume + " ");
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(voiceStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mediaPlayer.start();

        Uri uri = Uri.parse(voiceStoragePath);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(RecordFinalActivity.this, uri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        int millSecond = Integer.parseInt(durationStr);

        totalDuration = millSecond / 1000;
        ipv.setMax(totalDuration); // music duration in seconds.
        ipv.stop();
        ipv.start();
    }

    private void stopAudioPlay() {
        if (mediaPlayer != null) {
            ipv.stop();
            mediaPlayer.seekTo(0);
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void startPlaying() {
        if (!ipv.isPlaying()) {
            ipv.start();
        } else {
            ipv.stop();
        }

        fx.crossFader(100);
        fx.fxValue(0);
        fx.fxSelect(0);

        playing = true;

        // Sending fake touches every second helps sustaining CPU rate.
        // This is not necessary for this little app, but might be helpful for projects with big audio processing needs.
        if (playing) {
            TimerTask fakeTouchTask = new TimerTask() {
                public void run() {
                    try {
                        Instrumentation instrumentation = new Instrumentation();
                        instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACKSLASH);
                    } catch (Exception e) {
                        assert true;
                    }
                }
            };
            fakeTouchTimer = new Timer();
            fakeTouchTimer.schedule(fakeTouchTask, 1000, 1000);
        } else {
            fakeTouchTimer.cancel();
            fakeTouchTimer.purge();
        }

        fx.playPause(playing);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopAudioPlay();
        audioPlayer.stop();
        fx.playPause(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopAudioPlay();
        audioPlayer.stop();
        fx.playPause(false);
    }

    public void effectsSelected() {
        show_efects.setTextColor(Color.parseColor("#FFFFFF"));
        show_efects.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg));

        show_backgrounds.setBackgroundColor(Color.parseColor("#FFF8DC"));
        show_backgrounds.setTextColor(Color.parseColor("#9c8011"));
    }

    public void backgroundSelected() {
        show_backgrounds.setTextColor(Color.parseColor("#FFFFFF"));
        show_backgrounds.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg));

        show_efects.setBackgroundColor(Color.parseColor("#FFF8DC"));
        show_efects.setTextColor(Color.parseColor("#9c8011"));
    }

    public void startCountDown(final long totalLength) {
        //startAudioRecording();
        timer = new CountDownTimer((totalLength * 1000), 1000) {

            public void onTick(long millisUntilFinished) {
                Log.e("mill until finished", millisUntilFinished + "");
            }

            public void onFinish() {
                fx.fxOff();
                fx.playPause(false);
                stopAudioPlay();
                audioPlayer.stop();
                Log.e("Stopped playing", "length " + totalLength + " ");
            }


        }.start();
    }

    public void stopCountdown() {
        //stopAudioRecording();
        timer.cancel();
    }

    private void initializeMediaRecord() {
        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            mediaRecorder.setOutputFile(voiceRecordingPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startAudioRecording() {
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void restartRecording(){
        mediaRecorder.reset();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(voiceRecordingPath);
        startAudioRecording();
    }

    private void stopAudioRecording() {
        if (mediaRecorder != null) {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    private void hasSDCard() {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (isSDPresent) {
            System.out.println("There is SDCard");
        } else {
            System.out.println("There is no SDCard");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(RecordFinalActivity.this, RecordActivity.class);
        startActivity(i);
        finish();
    }

//    private native void SuperpoweredExample(int samplerate, int buffersize, String apkPath, int fileAoffset, int fileAlength, int fileBoffset, int fileBlength);
//
//    private native void onPlayPause(boolean play);
//
//    private native void onCrossfader(int value);
//
//    private native void onFxSelect(int value);
//
//    private native void onFxOff();
//
//    private native void onFxValue(int value);
//
//    static {
//        System.loadLibrary("SuperpoweredExample");
//    }
}
