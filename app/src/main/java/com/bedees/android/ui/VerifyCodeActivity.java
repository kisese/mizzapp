package com.bedees.android.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.ParseConstants;
import com.bedees.android.util.Utilities;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.util.HashMap;
import java.util.Map;

public class VerifyCodeActivity extends AppCompatActivity {

    private Firebase mFireBase;
    public static final String KEY_PHONE="key_phone_verify_code_activity";
    public static final String KEY_COUNTRY_CODE="key_country_code_verify_code_activity";

    public static final String TAG=VerifyCodeActivity.class.getSimpleName();

    public static final String PROGRESS_TITLE_VERIFYING_PHONE_NUMBER = "Verifying your phone number.";
    public static final String PROGRESS_TITLE_REGISTRATION = "Saving your profile.";
    private ProgressDialog mProgressDialog;

    private String phoneNumber;
    private String countryCode;

    private EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mFireBase = new Firebase(FirebaseConstants.FIRE_BASE_URL);

        phoneNumber = getIntent().getStringExtra(KEY_PHONE);
        countryCode = getIntent().getStringExtra(KEY_COUNTRY_CODE);

        mEditText = (EditText)findViewById(R.id.et_code);


    }

    public void onVerifyClick(View view){
        String code = mEditText.getText().toString();
        final String phoneNumber = this.countryCode+"-"+this.phoneNumber;
        String verificationNo = phoneNumber.replace("-","");
        
        showProgressDialog();

     /*   HashMap<String,String> params = new HashMap<>();
        params.put(ParseConstants.PARAM_VERIFICATION_NO,verificationNo);
        params.put(ParseConstants.PARAM_VERIFICATION_CODE,code);
*/
        boolean isVerified =Utilities.verifyVerificationCode(this,code);

        //TODO :for testing
       // isVerified = true;
        if(isVerified){
            //TODO: for testing

            //this.phoneNumber = "9872176103";
            //this.phoneNumber = "9646084419";
            //this.countryCode = "+91";

           Utilities.saveVerifiedPhoneNumber(this,this.phoneNumber,this.countryCode);

            Map<String,String> userData= new HashMap<>();
            userData.put("countryCode",this.countryCode);
            userData.put("phone",this.phoneNumber);


            /*Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("uid", "uniqueId1");
            payload.put("some", "arbitrary");
            payload.put("data", "here");
            TokenGenerator tokenGenerator = new TokenGenerator("<YOUR_FIREBASE_SECRET>");
            String token = tokenGenerator.createToken(payload);*/



            mFireBase.child("phone").child(countryCode+"-"+this.phoneNumber).setValue("1", new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError == null){




                        startActivity(new Intent(VerifyCodeActivity.this,ContactActivity.class));
                        finish();
                        Log.e(TAG,"firebase success");
                    }else{
                        Log.e(TAG,"firebase Error while saving ");
                    }
                }
            });


        }else{
            dismissProgressDialog();
            Toast.makeText(VerifyCodeActivity.this, "Not Verified", Toast.LENGTH_SHORT).show();

//            Intent i = new Intent(VerifyCodeActivity.this, ContactActivity.class);
//            startActivity(i);
        }

/*
        Log.e(TAG,"Verifying request for "+verificationNo+" with code "+code+" finally sent "+params.get(ParseConstants.PARAM_VERIFICATION_NO));
        ParseCloud.callFunctionInBackground(ParseConstants.FUNCTION_VERIFY_CODE, params, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {
                dismissProgressDialog();
                if(e ==  null){
                    Log.e(TAG,"Success "+object);
                    Utilities.saveVerifiedPhoneNumber(VerifyCodeActivity.this, numberToSave, countryCode);


                    registerInBackground();


                }else{
                    Toast.makeText(VerifyCodeActivity.this, "Registration Failed", Toast.LENGTH_SHORT).show();
                    Log.e(TAG,"Error "+e);
                }
            }
        });
*/


    }


    private boolean checkConnectivity() {

        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (!isConnected) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Connect to network or quit")
                    .setCancelable(false)
                    .setPositiveButton("Connect to Network", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    })
                    .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            VerifyCodeActivity.this.finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        Log.i(TAG, " Connectivity Checked Status:" + isConnected);
        return isConnected;
    }


    private void registerInBackground() {
        if (checkConnectivity())
        {

            String phoneNumber = Utilities.getVerifiedPhoneNumber(this);
            String countryCode = Utilities.getVerifiedCountryCode(this);
            ParseObject user = ParseObject.create(ParseConstants.CLASS_PHONE);
            user.put(ParseConstants.KEY_COUNTRY_CODE,countryCode);
            user.put(ParseConstants.KEY_PHONE_NUMBER, countryCode + "-" + phoneNumber);

            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            //installation.put(ParseConstants.KEY_USER, user);
            installation.put(ParseConstants.KEY_PHONE_NUMBER, countryCode + "-" + phoneNumber);
            installation.put(ParseConstants.KEY_COUNTRY_CODE, countryCode);
            installation.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                        if (e == null) {
                        dismissProgressDialog();
                        Utilities.appRegistered(getApplicationContext());
                        startActivity(new Intent(VerifyCodeActivity.this, ContactActivity.class));
                            VerifyCodeActivity.this.finish();
                    } else {
                        Utilities.makeToastLong(VerifyCodeActivity.this, "Registration Failed");
                        Log.e(TAG,"Registration Failed due to "+e.toString());
                    }

                }
            });
            user.saveInBackground();

        }else{
            Log.e(TAG," Network not available.");
            Toast.makeText(VerifyCodeActivity.this, "Network Not Available To Save Details", Toast.LENGTH_SHORT).show();
        }
    }


    private void showProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(PROGRESS_TITLE_VERIFYING_PHONE_NUMBER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        Log.i(TAG, "Progress Dialog Shown");
    }

    private void dismissProgressDialog() {
        try {
            Log.e(TAG, "Progress Dialog Dismissed.");
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }catch (Exception exp){
            Log.e(TAG, "Progress Dialog Dismissed Exception ."+exp);

        }
    }

    private void setProgressDialogStatus(String newStatus) {
        Log.i(TAG, "Progress Dialog status changed to " + newStatus);
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.setMessage(newStatus);
    }



}
