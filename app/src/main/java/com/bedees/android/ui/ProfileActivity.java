package com.bedees.android.ui;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bedees.android.R;
import com.bedees.android.manager.CameraManager;
import com.bedees.android.manager.FirebaseManager;
import com.bedees.android.model.ProfilePicture;
import com.bedees.android.util.DownloadStatus;
import com.bedees.android.util.FirebaseConstants;
import com.bedees.android.util.UploadStatus;
import com.bedees.android.util.Utilities;
import com.bumptech.glide.Glide;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import co.mobiwise.library.InteractivePlayerView;
import pub.devrel.easypermissions.EasyPermissions;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks {

    public static final String TAG = ProfileActivity.class.getSimpleName();
    static final String AB = "abcdefghijklmnopqrstuvwxyz";
    static Random rnd = new Random();
    String voiceStoragePath;
    MediaPlayer mediaPlayer;
    private int totalDuration = 123;
    private ImageView mIvProfilePicture;
    private TextView mTvDateOfStatus;
    private TextView mTvStatusText;
    private ImageButton mIbpause;
    private ImageButton mIbplay;
    private ImageButton mIbrecord;
    private ProgressBar mPb;
    private InteractivePlayerView progerssView;
    private MediaRecorder mediaRecorder;
    private String mCapturedImageStringURI = "";
    private boolean debug = true;
    private Firebase mFireBase;
    private CountDownTimer timer;
    private int progress;
    private Firebase mFirebaseRef;
    private static final String KEY_FILE_URI = "key_file_uri";
    private static final String KEY_DOWNLOAD_URL = "key_download_url";

    private BroadcastReceiver mDownloadReceiver;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;

    private Uri mDownloadUrl = null;
    private Uri mFileUri = null;

    // [START declare_ref]
    private StorageReference storageRef;
    // [END declare_ref]
    private UploadStatus uploadStatus;
    private DownloadStatus downloadStatus;
    private String imgage_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_profile);

        final ActionBar actionBar = ((ProfileActivity)this).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.hide();
        }

        mFireBase = new Firebase(FirebaseConstants.FIRE_BASE_URL);
        uploadStatus = new UploadStatus(this);
        downloadStatus = new DownloadStatus(this);

        hasSDCard();

        setUpToolBar();
        setUpViews();
        tryToGetProfilePicture();

        voiceStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File audioVoice = new File(voiceStoragePath + File.separator + "voices");
        if (!audioVoice.exists()) {
            audioVoice.mkdir();
        }
        voiceStoragePath = voiceStoragePath + File.separator + "voices/final" + generateVoiceFilename(6) + ".mp3";

        progerssView.setMax(totalDuration);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        // Initialize Firebase Storage Ref
        // [START get_storage_ref]
        // [END get_storage_ref]
        mFirebaseRef = new Firebase(FirebaseConstants.FIRE_BASE_URL);

//        mIbpause.setEnabled(false);
//        mIbplay.setEnabled(false);

        initializeMediaRecord();

        //TODO this is a test remove this

        //get firebase storage instance
        FirebaseStorage storage = FirebaseStorage.getInstance();

        // Create a storage reference from our app
        storageRef = storage.getReferenceFromUrl("gs://project-1958300502970119287.appspot.com");

        // Create a reference to "mountains.jpg"
        StorageReference mountainsRef = storageRef.child(voiceStoragePath);

        // Create a reference to 'images/mountains.jpg'
        StorageReference mountainImagesRef = storageRef.child("voice/" + voiceStoragePath);

        // While the file names are the same, the references point to different files
        mountainsRef.getName().equals(mountainImagesRef.getName());
        mountainsRef.getPath().equals(mountainImagesRef.getPath());
        System.out.println("mountainsRef.getName(): " + mountainsRef.getName().equals(mountainImagesRef.getName()));

        try {
            Uri uri = Uri.parse(voiceStoragePath);
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(ProfileActivity.this, uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int millSecond = Integer.parseInt(durationStr);

            totalDuration = millSecond / 1000;
            Log.e("Total Duration", totalDuration + " ");

            progerssView.setMax(totalDuration);

            if (uploadStatus.getUploadStatus() == false) {
                File file = new File(voiceStoragePath);
                mFileUri = Uri.fromFile(file);
                //uploadFromUri(mFileUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mIbpause.setEnabled(false);
            mIbplay.setEnabled(false);
        }

        // Restore instance state
        if (savedInstanceState != null) {
            mFileUri = savedInstanceState.getParcelable(KEY_FILE_URI);
            mDownloadUrl = savedInstanceState.getParcelable(KEY_DOWNLOAD_URL);
        }

        // Download receiver
        mDownloadReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "downloadReceiver:onReceive:" + intent);
                hideProgressDialog();

                if (MyDownloadService.ACTION_COMPLETED.equals(intent.getAction())) {
                    String path = intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH);
                    long numBytes = intent.getLongExtra(MyDownloadService.EXTRA_BYTES_DOWNLOADED, 0);

                    // Alert success
                    showMessageDialog("Success", String.format(Locale.getDefault(),
                            "%d bytes downloaded from %s", numBytes, path));
                }

                if (MyDownloadService.ACTION_ERROR.equals(intent.getAction())) {
                    String path = intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH);

                    // Alert failure
                    showMessageDialog("Error", String.format(Locale.getDefault(),
                            "Failed to download from %s", path));
                }
            }
        };

        signInAnonymously();

        try {
            if (downloadStatus.getDownloadStatus() == false)
                beginDownload();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up toolbar
     */
    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null && toolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    /**
     * Initializes views here
     */
    private void setUpViews() {
        mIvProfilePicture = (ImageView) findViewById(R.id.iv_profile_picture);
//        mTvDateOfStatus = (TextView) findViewById(R.id.tv_date_of_status);
//        mTvStatusText = (TextView) findViewById(R.id.tv_status_body);
        mIbplay = (ImageButton) findViewById(R.id.ib_play);
        mIbpause = (ImageButton) findViewById(R.id.ib_pause);
        mIbrecord = (ImageButton) findViewById(R.id.ib_start_recording);
        findViewById(R.id.rel_add_picture).setOnClickListener(this);
        mIbplay.setOnClickListener(this);
        mIbpause.setOnClickListener(this);
        mIbrecord.setOnClickListener(this);
        mPb = (ProgressBar) findViewById(R.id.pb_image_loading);
        progerssView = (InteractivePlayerView) findViewById(R.id.seek_audio_status);

        initProgressDialog();
    }

    /**
     * Tries to retrieve profile picture basing on registered phone number
     */
    private void tryToGetProfilePicture() {
        mPb.setVisibility(View.VISIBLE);
        FirebaseManager.getInstance().getProfilePictureForPhoneNumber(Utilities.getVerifiedNormalizedPhoneNumber(ProfileActivity.this),
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            ProfilePicture picture = dataSnapshot.getValue(ProfilePicture.class);
                            if (picture != null) {
                                Glide.with(ProfileActivity.this).load(picture.getUrl()).into(mIvProfilePicture);
                                imgage_url = picture.getUrl();
                                progerssView.setCoverURL(imgage_url);
                            }
                            mPb.setVisibility(View.GONE);
                        } catch (Exception e) {
                            mPb.setVisibility(View.GONE);
                            ERROR(e.toString());
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
    }

    /**
     * Initializes ProgressDialog
     */
    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Please wait");
    }

    /**
     * Displays captured by Camera photo in ImageView,
     * uploads on FirebaseStorage  after Picasso properly scales it
     */
    private void showAndUploadTakenPhoto() {
        if (!TextUtils.isEmpty(mCapturedImageStringURI)) {
            mProgressDialog.show();
            Picasso.with(this).load(mCapturedImageStringURI)
                    .resize(600, 600)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mIvProfilePicture.setImageBitmap(bitmap);
                            addImageToFireBaseStorage(bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            ERROR("showAndUploadTakenPhoto :: onBitmapFailed");
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
        }
    }

    /**
     * Uploads captured image on FirebaseStorage
     *
     * @param bitmap - Bitmap
     */
    private void addImageToFireBaseStorage(Bitmap bitmap) {
        if (bitmap != null) {
            FirebaseManager.getInstance().uploadNewProfilePicture(bitmap,
                    Utilities.getVerifiedNormalizedPhoneNumber(this),
                    new FirebaseManager.OnImageUploadedListener() {
                        @Override
                        public void onImageUploaded(String url) {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void onUploadFailed() {
                            mProgressDialog.dismiss();
                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CameraManager.ACTION_TAKE_CAMERA_PHOTO:
                if (resultCode == RESULT_OK) {
                    mCapturedImageStringURI = CameraManager.getInstance().getCurrentPhotoPathUri();
                }
                break;
            case CameraManager.ACTION_TAKE_GALLERY_PHOTO:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        if (data.getData() != null) {
                            mCapturedImageStringURI = data.getData().toString();
                        }
                    }
                }
                break;
        }
        showAndUploadTakenPhoto();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        CameraManager.getInstance().onPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ib_play:
                Toast.makeText(ProfileActivity.this, "Play", Toast.LENGTH_SHORT).show();
                playLastStoredAudioMusic();
                mediaPlayerPlaying();
                startCountDown(totalDuration);
                progerssView.start();
                break;
            case R.id.ib_pause:
                Toast.makeText(ProfileActivity.this, "Pause", Toast.LENGTH_SHORT).show();
                //stopAudioRecording();
                stopAudioPlay();
                progerssView.stop();
                break;
            case R.id.rel_add_picture:
                CameraManager.getInstance()
                        .launchAlertChooser(this);
                break;
            case R.id.ib_start_recording:
                Toast.makeText(ProfileActivity.this, "Record", Toast.LENGTH_SHORT).show();
//                if (mediaRecorder == null) {
//                    initializeMediaRecord();
//                }
//                startAudioRecording();
                Intent i = new Intent(ProfileActivity.this, RecordActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }

    private void ERROR(String msg) {
        if (debug) {
            Log.e(TAG, msg);
        }
    }

    private void COMPLAIN(String msg) {
        if (debug) {
            Log.d(TAG, msg);
        }
    }

    private String generateVoiceFilename(int len) {
//        StringBuilder sb = new StringBuilder(len);
//        for (int i = 0; i < len; i++)
//            sb.append(AB.charAt(rnd.nextInt(AB.length())));
//        return sb.toString();

        //return "voice";
        return Utilities.getVerifiedNormalizedPhoneNumber(ProfileActivity.this);
    }

    private void startAudioRecording() {
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mIbrecord.setEnabled(false);
        mIbpause.setEnabled(true);
    }

    private void stopAudioRecording() {
        if (mediaRecorder != null) {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
        }
        mIbpause.setEnabled(false);
        mIbplay.setEnabled(true);
    }

    private void playLastStoredAudioMusic() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(voiceStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
//        mIbrecord.setEnabled(true);
//        mIbplay.setEnabled(false);
    }

    private void stopAudioPlay() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    private void hasSDCard() {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (isSDPresent) {
            System.out.println("There is SDCard");
        } else {
            System.out.println("There is no SDCard");
        }
    }

    private void mediaPlayerPlaying() {
        if (!mediaPlayer.isPlaying()) {
            stopAudioPlay();
        }
    }

    private void initializeMediaRecord() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(voiceStoragePath);
    }


    public void startCountDown(final long totalLength) {
        //startAudioRecording();
        timer = new CountDownTimer((totalLength * 1000), 1000) {

            public void onTick(long millisUntilFinished) {
                Log.e("mill until finished", millisUntilFinished + "  - " + (int) (millisUntilFinished / 1000));
                progress = (int) (millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.e("Stopped playing", "length " + totalLength + " ");
                progerssView.stop();
                progerssView.setProgress(0);
            }


        }.start();
    }

    // [START upload_from_uri]
    private void uploadFromUri(Uri fileUri) {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());

        // [START get_child_ref]
        // Get a reference to store file at photos/<FILENAME>.jpg
        final StorageReference photoRef = storageRef.child("voices")
                .child(fileUri.getLastPathSegment());
        // [END get_child_ref]

        // Upload file to Firebase Storage
        // [START_EXCLUDE]
        showProgressDialog();
        // [END_EXCLUDE]
        Log.d(TAG, "uploadFromUri:dst:" + photoRef.getPath());
        photoRef.putFile(fileUri)
                .addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Upload succeeded
                        Log.d(TAG, "uploadFromUri:onSuccess");

                        // Get the public download URL
                        mDownloadUrl = taskSnapshot.getMetadata().getDownloadUrl();

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        uploadStatus.setUploadStatus(true);
                        //updateUI(mAuth.getCurrentUser());
                        // [END_EXCLUDE]
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Upload failed
                        Log.w(TAG, "uploadFromUri:onFailure", exception);

                        mDownloadUrl = null;

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        Toast.makeText(ProfileActivity.this, "Error: upload failed",
                                Toast.LENGTH_SHORT).show();
                        //updateUI(mAuth.getCurrentUser());
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END upload_from_uri]

    private void signInAnonymously() {
        // Sign in anonymously. Authentication is required to read or write from Firebase Storage.
        showProgressDialog();
        mAuth.signInAnonymously()
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Log.d(TAG, "signInAnonymously:SUCCESS");
                        hideProgressDialog();
                        Log.e("AUTH RESULT", authResult.getUser().toString());
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e(TAG, "signInAnonymously:FAILURE", exception);
                        hideProgressDialog();
                        //updateUI(null);
                    }
                });
    }

    private void beginDownload() {
        // Get path
        String path = "voices/" + mFileUri.getLastPathSegment();

        // Kick off download service
        Intent intent = new Intent(this, MyDownloadService.class);
        intent.setAction(MyDownloadService.ACTION_DOWNLOAD);
        intent.putExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH, path);
        startService(intent);

        Toast.makeText(ProfileActivity.this, "Downloading voice", Toast.LENGTH_LONG).show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                try {
//                    String filePath = "voices/" + mFileUri.getLastPathSegment();
//                    mediaPlayer = new MediaPlayer();
//                    mediaPlayer.setDataSource(filePath);
//                    mediaPlayer.prepare();
//                    mediaPlayer.start();
//                    progerssView.start();
                    Log.e("Playing", "Playing");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2000);

        downloadStatus.setDownloadStatus(true);

        // Show loading spinner
        showProgressDialog();
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showMessageDialog(String title, String message) {
        AlertDialog ad = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .create();
        ad.show();
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
        super.onSaveInstanceState(out);
        out.putParcelable(KEY_FILE_URI, mFileUri);
        out.putParcelable(KEY_DOWNLOAD_URL, mDownloadUrl);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
    }
}

