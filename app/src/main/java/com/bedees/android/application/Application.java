package com.bedees.android.application;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bedees.android.model.Contact;
import com.bedees.android.util.ParseConstants;
import com.firebase.client.Firebase;
import com.google.firebase.FirebaseApp;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MOHIT on 02-01-2016.
 */
public class Application extends android.app.Application {
    public static List<Contact> mContactList= new ArrayList<>();
    public static final String TAG = Application.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static Application mInstance;

    @Override
    public void onCreate() {
        super.onCreate();


        mInstance=this;
        // Enable Local Datastore.
      //  ParseObject.registerSubclass(Item.class);
      //  ParseObject.registerSubclass(Order.class);
      //  ParseObject.registerSubclass(ItemOrdered.class);
        String applicationId, clientKey;

        Firebase.setAndroidContext(this);

        if (!FirebaseApp.getApps(this).isEmpty())
        Firebase.getDefaultConfig().setPersistenceEnabled(true);

//        Parse.enableLocalDatastore(this);
  //      Parse.initialize(this, "xSj8L7Ep86xiRnwlHr67W9myz11h8WKna3xz1FNp", "UuNVO2XOaCsTHmbELWT3Et2H0AkW8zIDnhd83aGc");
    //    ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
          //  @Override
        //    public void done(ParseException e) {
      //          new AsyncTask<Void,Void,Void>(){
      //              @Override
    //                protected Void doInBackground(Void... params) {
                        //getAllContacts();
        //                return null;
          //          }
            //    }.execute();

//            }
  //      });
//        Parse.initialize(this, "7c5Mh3O8D6PWgklqxtiALQeD5Gtu2o4X9JiUH0DB", "ZqQPTrxqRUKNKuUii3aRJaUtNSjc0C7UFnXlObtD");

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized Application getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    private void getAllContacts(){
        Cursor sortedCursor = getContentResolver()
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        new String[]{

                                ContactsContract.CommonDataKinds.Phone._ID,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                ContactsContract.CommonDataKinds.Phone.NUMBER

                                // ContactsContract.Data.PHOTO_URI
                        }, null,
                        null, "lower(" + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + ")"
                );

        extractContactsFromCursor(sortedCursor);
        sortedCursor.close();
        filterDataFromServer(mContactList);

    }
    private void filterDataFromServer(List<Contact> contactList){
        List<String> contactNumberList = new ArrayList<>();
        final HashMap<String,Contact> contactHashMap = new HashMap<>();
        for(int i=0;i<contactList.size();i++){
            Contact contact = contactList.get(i);
            if(contact == null ) continue;
            String phoneNumber = contact.getPhoneNumber();
            phoneNumber =phoneNumber.replaceAll("\\s", "");
            phoneNumber =phoneNumber.replaceAll("\\+", "");
            phoneNumber =phoneNumber.replaceAll("-", "");
            if(!contactNumberList.contains(phoneNumber)){
                contactNumberList.add(phoneNumber);
                contactHashMap.put(phoneNumber, contact);
            }
        }
        HashMap<String,Object> params = new HashMap<>();
        params.put(ParseConstants.KEY_CONTACTS, contactNumberList);
        Log.e(TAG, "Contacts Passed " + contactNumberList.size());
        //All Numbers are in contactNumberList
        mContactList.clear();
        ParseCloud.callFunctionInBackground(ParseConstants.FUNCTION_CONTACT_INSTALLATION, params, new FunctionCallback<Object>() {
            @Override
            public void done(Object object, ParseException e) {

                if (e == null) {
                    String className = object.getClass().toString();
                    Log.e(TAG, "Installation Class Name " + className);
                    Log.e(TAG, "Got Installations " + object.toString());
                    if (object instanceof ArrayList) {
                        ArrayList list = (ArrayList) object;
                        Log.e(TAG, "List Length " + list.size());

                        for (int j = 0; j < list.size(); j++) {
                            try {
                                Log.e(TAG, list.get(j) + "  of Class " + list.get(j).getClass().toString());
                                ParseObject curObject = (ParseObject) list.get(j);

                                Log.e(TAG, "map " + curObject.keySet());
                                String curPhoneNumber = curObject.fetchIfNeeded().getString("PhoneNumber");
                                String curCountryCode = curObject.fetchIfNeeded().getString("CountryCode");
                                String normalizedPhoneNumber = new String(curPhoneNumber);

                                curPhoneNumber = curPhoneNumber.replaceAll("-", "");
                                curPhoneNumber = curPhoneNumber.replaceAll("\\+", "");
                                curCountryCode = curCountryCode.replaceAll("-", "");
                                curCountryCode = curCountryCode.replaceAll("\\+", "");
                                Contact contact = null;
                                if (curPhoneNumber.startsWith(curCountryCode)) {
                                    curPhoneNumber = curPhoneNumber.substring(curCountryCode.length());
                                }

                                if (contactHashMap.containsKey(curPhoneNumber)) {
                                    Log.e(TAG, "Got Key " + curPhoneNumber);
                                    contact = contactHashMap.get(curPhoneNumber);
                                    contact.setCountryCode(curCountryCode);
                                    contact.setNormalizedNumber(normalizedPhoneNumber);
                                    mContactList.add(contact);
                                } else if (contactHashMap.containsKey(curCountryCode + curPhoneNumber)) {
                                    Log.e(TAG, "Got Key " + curCountryCode + curPhoneNumber);
                                    contact = contactHashMap.get(curCountryCode + curPhoneNumber);
                                    contact.setNormalizedNumber(normalizedPhoneNumber);
                                    mContactList.add(contact);
                                } else {
                                    Log.e(TAG, "Failed to Get Key " + curPhoneNumber + " or " + curCountryCode + curPhoneNumber + " in hashmap " + contactHashMap.keySet());
                                }
                            } catch (Exception exp) {
                                Log.e(TAG, "Exceptin " + exp);
                            }
                        }
                        //here
                        Log.e(TAG, " Finally We have " + mContactList.size() + " contacts");
                    }
                } else {
                    Log.e(TAG, "Exception to get Installations " + e.toString(), e);
                }
            }
        });
//        ParseInstallation.getQuery().ContainedIn(Utilities.KEY_VERIFIED_PHONE_NUMBER, contactNumberList);
    }

    private void extractContactsFromCursor(Cursor cursor){
        if(cursor!=null){
            Log.e(TAG, "Cursor not null");
            while(cursor.moveToNext()){
                String id = cursor.getString(0);
                String name = cursor.getString(1);
                String number = cursor.getString(2);
                Contact contact = new Contact();
                contact.setContactId(id);
                contact.setName(name);
                contact.setPhoneNumber(number);
                mContactList.add(contact);
                Log.e(TAG,"Contact "+contact.getName());
            }
        }
    }


}

