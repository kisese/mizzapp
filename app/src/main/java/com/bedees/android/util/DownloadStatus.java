package com.bedees.android.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Brayo on 9/6/2016.
 */
public class DownloadStatus {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "download_status";

    public DownloadStatus(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setDownloadStatus(Boolean download_status) {
        editor.putBoolean("download_status", download_status);
        editor.commit();
    }


    public Boolean getDownloadStatus(){
        Boolean login_status = pref.getBoolean("download_status", false);
        return login_status;
    }

    public void clearAll(){
        editor.clear();
        editor.commit();
    }
}
