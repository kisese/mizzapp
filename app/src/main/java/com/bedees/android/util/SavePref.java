package com.bedees.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.bedees.android.model.Contact;

import java.util.ArrayList;

public class SavePref {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	Context con;

	public SavePref(Context c) {

		con = c;
		preferences = PreferenceManager.getDefaultSharedPreferences(con);
		editor = preferences.edit();
	}

	public void setPhoneNumber(String phnum) {
		editor.putString("phone_num", phnum);
		editor.commit();

	}

	public String getPhoneNumber() {
		String name = preferences.getString("phone_num", "");
		return name;
	}

	public void setAppStatus(String status) {
		editor.putString("status", status);
		editor.commit();

	}

	public String getAppStatus() {
		String status = preferences.getString("status", "");
		return status;
	}

	public void setServiceStatus(String serviceStatus) {
		editor.putString("serviceStatus", serviceStatus);
		editor.commit();

	}

	public String getServiceStatus() {
		String serviceStatus = preferences.getString("serviceStatus", "");
		return serviceStatus;
	}



	public void setCaller_name(String caller_name) {
		editor.putString("caller_name", caller_name);
		editor.commit();

	}

	public String getCaller_name() {
		String caller_name = preferences.getString("caller_name", "");
		return caller_name;
	}


	public void setNotificationId(int notificationId) {
		editor.putInt("notificationId", notificationId);
		editor.commit();

	}

	public Integer getNotificationId() {
		int notificationId = preferences.getInt("notificationId", 0);
		return notificationId;
	}

	public void setContactList(ArrayList<Contact> contactList) {

		editor.putString("contactList", ObjectSerializer.serialize(contactList));
		editor.commit();
	}

	public ArrayList<Contact> getContactList() {

		ArrayList<Contact> contactList = new ArrayList<>();
		contactList = (ArrayList) ObjectSerializer.deserialize(
				preferences.getString("contactList", ObjectSerializer.serialize(new ArrayList())));


		return contactList;
	}

}