package com.bedees.android.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.bedees.android.R;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by MOHIT on 02-01-2016.
 */
public class Utilities {

    //8475981191  This is mobile number

    public static final String IS_REGISTERED = "is_app_registered";
    public static final String KEY_VERIFICATION_CODE = "key_verification_code";


    public static final String KEY_VERIFIED_PHONE_NUMBER = "key_verified_phone_number";
    public static final String KEY_VERIFIED_COUNTRY_CALLING_CODE = "key_verified_country_calling_code";    /**
     * Shows the Toast with length Toast.LENGTH_LONG with given context and given message.
     *
     * @param context
     * @param message
     */
    public static void makeToastLong(final Context context, final String message) {

        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Run your task here
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        }, 100);
    }

    /**
     * Shows the Toast with length Toast.LENGTH_SHORT with given context and given message.
     *
     * @param context
     * @param message
     */
    public static void makeToastShort(final Context context, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Run your task here
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        }, 100);

    }

    public static String formatDateTime(Long dateTime) {
        String result = null;
        Date date = new Date();
        Long curDateTime = date.getTime();
        Long timePassed = curDateTime - dateTime;
        if (timePassed > 2 * 24 * 60 * 60 * 1000L) {
            //   Date returnDate = new Date(dateTime);
            // returnDate.getTime()
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(dateTime);
            result = String.format("%02d", c.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", c.get(Calendar.MONTH))
                    + "-" + String.format("%02d", (c.get(Calendar.YEAR) % 100));
        } else if (timePassed > 24 * 60 * 60 * 1000L) {
            result = "yesterday";
        } else

        {
            if (timePassed >= 60 * 60 * 1000)//>= 1hour
            {
                //hours
                result = "" + (timePassed / (60 * 60 * 1000L));
                result += " hours ago";
            } else {
                //minutes
                result = "" + (timePassed / (60 * 1000L));
                result += " mins ago";
            }
        }
        return result;

    }
    public static boolean isIntentSafe(Context context, Intent intent) {
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;
        return isIntentSafe;
    }

    //added after backend
    public static boolean isAppRegistered(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean isAppRegistered = sharedPreferences.getBoolean(IS_REGISTERED, false);

        return isAppRegistered;
    }

    public static void appRegistered(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_REGISTERED, true);
        editor.commit();
        editor.apply();
    }

    public static boolean verifyVerificationCode(Context context,String enteredVerificationCode){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String verificationCode= sharedPreferences.getString(KEY_VERIFICATION_CODE, "");
        if(verificationCode.equals(enteredVerificationCode)){
            appRegistered(context);
            return  true;
        }
        return false;
    }

    public static String generateAndSaveVerificationCode(Context context) {
        SecureRandom random = new SecureRandom();
        String verificationCode = "";
        String letters ="0123456789";//letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789+@";
        for (int i = 0; i < 5; i++) {
            int index = (int) (random.nextDouble() * letters.length());
            index %= letters.length() - 2;
            verificationCode += letters.substring(index, index + 1);
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_VERIFICATION_CODE, verificationCode);
        editor.commit();
        editor.apply();
        return verificationCode;
    }

    public static void saveVerifiedPhoneNumber(Context context, String phoneNumber, String countryCallingCode) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_VERIFIED_PHONE_NUMBER, phoneNumber);
        editor.putString(KEY_VERIFIED_COUNTRY_CALLING_CODE, countryCallingCode);
        editor.commit();
        editor.apply();
    }

    public static String getVerifiedPhoneNumber(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(KEY_VERIFIED_PHONE_NUMBER, "");
    }


    public static String getVerifiedNormalizedPhoneNumber(Context context) {
        return getVerifiedCountryCode(context)+"-"+getVerifiedPhoneNumber(context);
    }

    public static String getVerifiedCountryCode(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(KEY_VERIFIED_COUNTRY_CALLING_CODE, "");
    }

    public static String getCountryZipCode(Context context) {
        String CountryID = "";
        String CountryZipCode = "";

        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        String[] rl = context.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }


    public static boolean isInternetConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        return isConnected;
    }


    public static String getContactNameFromNumber(Context context, String newPhoneNumber){
        String[] projection = new String[]{
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID,
                ContactsContract.PhoneLookup.NORMALIZED_NUMBER

        };
        Uri contactUri= Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(newPhoneNumber));
        ContentResolver cr = context.getContentResolver();
        Cursor crc= cr.query(contactUri, projection, null, null, null);
        String name,id,number;
        if(crc.moveToNext()){
            name=crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            id = crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup._ID));
            number = crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.NORMALIZED_NUMBER));

        }   else{
            name ="Unknown";
        }

        return name;
    }




}
