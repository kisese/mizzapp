package com.bedees.android.util;

/**
 * Created by mk on 4/24/2016.
 */
public class Api {

    //public static final String BASE_URL = "https://www.augustguest.com/old_augustguest_10_13_2014/admin/api/";
    public static final String CRICKET= "http://cricapi.com/api/cricket";
    public static final String CRICKET_SCORE= "http://cricapi.com/api/cricketScore?unique_id=";

    public static final String Favquat= "https://favqs.com/api/qotd";
    public static final String Catfact= "http://catfacts-api.appspot.com/api/facts";
}
