package com.bedees.android.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Brayo on 9/6/2016.
 */
public class UploadStatus {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "upload_status";

    public UploadStatus(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setUploadStatus(Boolean upload_status) {
        editor.putBoolean("upload_status", upload_status);
        editor.commit();
    }


    public Boolean getUploadStatus(){
        Boolean login_status = pref.getBoolean("upload_status", false);
        return login_status;
    }

    public void clearAll(){
        editor.clear();
        editor.commit();
    }
}
