package com.bedees.android.util;

/**
 * Created by MOHIT on 02-01-2016.
 */
public class ParseConstants {
    public static final String CLASS_PHONE="Phone";
    public static final String KEY_PHONE_NUMBER="PhoneNumber";
    public static final String KEY_SENDER="Sender";
    public static final String KEY_MESSAGE="message";
    public static final String KEY_COUNTRY_CODE="CountryCode";
    public static final String KEY_USER="User";
    public static final String KEY_CONTACTS="contacts";

    public static final String FUNCTION_CONTACT_INSTALLATION="ContactInstallations";
    public static final String FUNCTION_SEND_PUSH="SendPush";
    public static final String FUNCTION_REQUEST_CODE="RequestCode";

    public static final String PARAM_VERIFICATION_NO ="PhoneNumber";
    public static final String PARAM_VERIFICATION_CODE ="Code";
    public static final String FUNCTION_VERIFY_CODE="VerifyCode";



}
