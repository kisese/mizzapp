package com.bedees.android.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by li010 on 19/09/14.
 */
public class TimeUtils {
    public final static long ONE_SECOND = 1000;
    public final static long SECONDS = 60;

    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long MINUTES = 60;

    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long HOURS = 24;

    public final static long ONE_DAY = ONE_HOUR * 24;

    private final static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private TimeUtils() {
    }

    static {
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
    /**
     * converts time (in milliseconds) to human-readable format
     * "<w> days, <x> hours, <y> minutes and (z) seconds"
     */
//    public static String millisToLongDHMS(long duration) {
//        StringBuilder res = new StringBuilder();
//        long temp = 0;
//        if (duration >= ONE_SECOND) {
//            temp = duration / ONE_DAY;
//            if (temp > 0) {
//                duration -= temp * ONE_DAY;
//                res.append(temp).append(" day").append(temp > 1 ? "s" : "")
//                        .append(duration >= ONE_MINUTE ? ", " : "");
//            }
//
//            temp = duration / ONE_HOUR;
//            if (temp > 0) {
//                duration -= temp * ONE_HOUR;
//                res.append(temp).append(" hour").append(temp > 1 ? "s" : "")
//                        .append(duration >= ONE_MINUTE ? ", " : "");
//            }
//
//            temp = duration / ONE_MINUTE;
//            if (temp > 0) {
//                duration -= temp * ONE_MINUTE;
//                res.append(temp).append(" minute").append(temp > 1 ? "s" : "");
//            }
//
//            if (!res.toString().equals("") && duration >= ONE_SECOND) {
//                res.append(" and ");
//            }
//
//            temp = duration / ONE_SECOND;
//            if (temp > 0) {
//                res.append(temp).append(" second").append(temp > 1 ? "s" : "");
//            }
//            return res.toString();
//        } else {
//            return "Just Now";
//        }
//    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        final long diff = now - time;
        if (diff < ONE_MINUTE) {
            return "just now";
        } else if (diff < 2 * ONE_MINUTE) {
            return "a minute ago";
        } else if (diff < 50 * ONE_MINUTE) {
            return diff / ONE_MINUTE + " minutes ago";
        } else if (diff < 90 * ONE_MINUTE) {
            return "an hour ago";
        } else if (diff < 24 * ONE_HOUR) {
            return diff / ONE_HOUR + " hours ago";
        } else if (diff < 48 * ONE_HOUR) {
            return "yesterday";
        } else {
            return diff / ONE_DAY + " days ago";
        }
    }


    /*public static void main(String args[]) {
        System.out.println(millisToLongDHMS(123));
        System.out.println(millisToLongDHMS((5 * ONE_SECOND) + 123));
        System.out.println(millisToLongDHMS(ONE_DAY + ONE_HOUR));
        System.out.println(millisToLongDHMS(ONE_DAY + 2 * ONE_SECOND));
        System.out.println(millisToLongDHMS(ONE_DAY + ONE_HOUR + (2 * ONE_MINUTE)));
        System.out.println(millisToLongDHMS((4 * ONE_DAY) + (3 * ONE_HOUR)
                + (2 * ONE_MINUTE) + ONE_SECOND));
        System.out.println(millisToLongDHMS((5 * ONE_DAY) + (4 * ONE_HOUR)
                + ONE_MINUTE + (23 * ONE_SECOND) + 123));
        System.out.println(millisToLongDHMS(42 * ONE_DAY));
        *//*
          output :
                0 second
                5 seconds
                1 day, 1 hour
                1 day and 2 seconds
                1 day, 1 hour, 2 minutes
                4 days, 3 hours, 2 minutes and 1 second
                5 days, 4 hours, 1 minute and 23 seconds
                42 days
         *//*
    }*/

    public static Date stringToDate(final String dateString){
        return TimeUtils.stringToDate(dateString, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date stringToDate(final String dateString, String dateformat){
        if(dateformat == null)
            dateformat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date formattedDate = null;
        try {
            formattedDate = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String formatDateString(String dateString, String dateFormat){
        if(dateFormat == null)
            dateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(TimeUtils.stringToDate(dateString));
    }

    public static String getDateAgo(final String dateString){
        String returnString = null;
        String formattedDateString = TimeUtils.formatDateString(dateString, "yyyy-MM-dd");
        if(formattedDateString.equals(TimeUtils.getDateAhead(0))) {
            returnString = "Today";
        }
        else if(formattedDateString.equals(TimeUtils.getDateAhead(-1))) {
            returnString = "Yesterday";
        }
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(TimeUtils.stringToDate(dateString).getTime());
            returnString = TimeUtils.getMonthName(calendar.get(Calendar.MONTH)) + " " + calendar.get(Calendar.DATE) + " " + calendar.get(Calendar.YEAR);
        }
        return returnString;
    }

    private static String getDateAhead(int dayAhead) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        if(dayAhead == -1)
            cal.add(Calendar.DATE, -1);
        else
            cal.add(Calendar.DATE, dayAhead);
        return dateFormat.format(cal.getTime());
    }
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static String getMonthName(int month){
        switch (month){
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
        }
        return null;
    }

    public static String convertToUserTimeZoneDate(String dateString){
        String returnDateString = "";
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        Date dt = TimeUtils.stringToDate(dateString, dateFormat);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(TimeUtils.getUserTimeZone());
        returnDateString = sdf.format(dt);
        return returnDateString;
    }

    public static TimeZone getUserTimeZone(){
        Calendar cal = Calendar.getInstance();
        long milliDiff = cal.get(Calendar.ZONE_OFFSET);
        // Got local offset, now loop through available timezone id(s).
        String[] ids = TimeZone.getAvailableIDs();
        TimeZone timeZone = null;
        for (String id : ids) {
            TimeZone tz = TimeZone.getTimeZone(id);
            if (tz.getRawOffset() == milliDiff) {
                // Found a match.
                timeZone = tz;
                break;
            }
        }
        return timeZone;
    }
}
