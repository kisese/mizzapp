package com.bedees.android.util;

/**
 * Created by Brayo on 8/6/2016.
 */
import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public class AudioPlayer {

    private MediaPlayer mMediaPlayer;

    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.seekTo(0);
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public void play(Context c, int rid) {
        stop();
        mMediaPlayer = MediaPlayer.create(c, rid);
        //mMediaPlayer.setLooping(true);
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stop();
            }
        });

        mMediaPlayer.start();
    }

    public void playAtVolume(Context c, int rid, int currVolume) {
        stop();
        int maxVolume = 100;
        mMediaPlayer = MediaPlayer.create(c, rid);
        mMediaPlayer.setLooping(true);
        final float volume = (float) (1 - (Math.log(maxVolume - currVolume) / Math.log(maxVolume)));
        mMediaPlayer.setVolume(volume, volume);
        Log.e("Audio Effects Volume ", volume + " ");
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                //stop();
            }
        });

        mMediaPlayer.start();
    }
}