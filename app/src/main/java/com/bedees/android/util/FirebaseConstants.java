package com.bedees.android.util;

/**
 * Created by MOHIT on 01-02-2016.
 */
public class FirebaseConstants {
    public static final String FIRE_BASE_URL = "https://mizzcall.firebaseio.com/";
    //    public static final String FIRE_BASE_URL = "https://vivid-torch-2582.firebaseio.com/";
    public static final String STORAGE_REF = "gs://project-1958300502970119287.appspot.com";


    public static final String CLASS_PHONE = "Phone";
    public static final String KEY_PHONE_NUMBER = "PhoneNumber";
    public static final String KEY_SENDER = "Sender";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_COUNTRY_CODE = "CountryCode";
    public static final String KEY_USER = "User";
    public static final String KEY_CONTACTS = "contacts";

    public static final String FUNCTION_CONTACT_INSTALLATION = "ContactInstallations";
    public static final String FUNCTION_SEND_PUSH = "SendPush";
    public static final String FUNCTION_REQUEST_CODE = "RequestCode";

    public static final String PARAM_VERIFICATION_NO = "PhoneNumber";
    public static final String PARAM_VERIFICATION_CODE = "Code";
    public static final String FUNCTION_VERIFY_CODE = "VerifyCode";

}
