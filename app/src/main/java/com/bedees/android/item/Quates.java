package com.bedees.android.item;

/**
 * Created by dhiraj on 10/5/16.
 */
public class Quates {


        private String unique_id;
        private String body;
        private String author;

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
