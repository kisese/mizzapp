package com.bedees.android.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;

import com.bedees.android.model.Contact;

import java.util.ArrayList;

/**
 * Created by MOHIT on 03-01-2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    // Contact Table Columns names


    public static final String DATABASE_NAME = "MizCall.db";
    //TODO: Change the data-type of datetime field to INTEGER
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String CHAR_TYPE = " CHAR(50)";
    private static final String CHAR_TYPE_LARGE = " CHAR(100)";
    private static final String BOOL_TYPE = " BOOLEAN";
    private static final String DATETIME_TYPE = " INTEGER";//changed from DATETIME
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_LOG =
            "CREATE TABLE " + Contract.Log.TABLE_NAME + " (" +
                    Contract.Log._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT  " + COMMA_SEP +
                    Contract.Log.COLUMN_NAME_DIRECTION+ CHAR_TYPE+ COMMA_SEP +
                    Contract.Log.COLUMN_NAME_NAME+ CHAR_TYPE_LARGE+ COMMA_SEP +
                    Contract.Log.COLUMN_NAME_NUMBER+ CHAR_TYPE+ COMMA_SEP +
                    Contract.Log.COLUMN_NAME_COUNTRY_CODE+ CHAR_TYPE+ COMMA_SEP +
                    Contract.Log.COLUMN_NAME_TIME+ DATETIME_TYPE+
                    " )";

    private static final String KEY_ID = "_id";
    private static final String KEY_LID = "_lid";
    private static final String KEY_NR_NUMBER = "NR_Number";
    private static final String KEY_NAME= "name";
    private static final String KEY_COUNTRYCODE = "country_code";
    private static final String KEY_NUMBER="number";

    private static final String CREATE_CART_TABLE = "CREATE TABLE " +
            Contract.Log.TABLE_Contact +
            "("
            +
            KEY_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT,"+
            KEY_LID +
            " INTEGER,"
            + KEY_NAME +
            " TEXT,"
            +KEY_NR_NUMBER +
            " TEXT," +
            KEY_COUNTRYCODE +
            " TEXT, "+
            KEY_NUMBER +
            " TEXT " +
             ")";


    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_LOG);
        db.execSQL(CREATE_CART_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        db.execSQL("DROP TABLE IF EXISTS " + Contract.Log.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Contract.Log.TABLE_Contact);
        onCreate(db);
    }



    public void callDialed(String number,String name,String countryCode){
        ContentValues contentValues = new ContentValues();
        long time = System.currentTimeMillis()/1000;
        String direction = Contract.Log.OUTGOING;

        contentValues.put(Contract.Log.COLUMN_NAME_DIRECTION,direction);
        contentValues.put(Contract.Log.COLUMN_NAME_NUMBER,number);
        contentValues.put(Contract.Log.COLUMN_NAME_TIME,time);
        contentValues.put(Contract.Log.COLUMN_NAME_NAME,name);
        contentValues.put(Contract.Log.COLUMN_NAME_COUNTRY_CODE,countryCode);

        SQLiteDatabase db =getWritableDatabase();
        db.insert(Contract.Log.TABLE_NAME, null, contentValues);
        db.close();
    }


    public void callMissed(String number,String name,String countryCode){
        ContentValues contentValues = new ContentValues();
        long time = System.currentTimeMillis()/1000;
        String direction = Contract.Log.INCOMING;

        contentValues.put(Contract.Log.COLUMN_NAME_DIRECTION,direction);
        contentValues.put(Contract.Log.COLUMN_NAME_NUMBER,number);
        contentValues.put(Contract.Log.COLUMN_NAME_TIME,time);
        contentValues.put(Contract.Log.COLUMN_NAME_NAME,name);
        contentValues.put(Contract.Log.COLUMN_NAME_COUNTRY_CODE,countryCode);

        SQLiteDatabase db =getWritableDatabase();
        db.insert(Contract.Log.TABLE_NAME, null, contentValues);
        db.close();
    }


    public void callReceived(String number,String name,String countryCode){
        ContentValues contentValues = new ContentValues();
        long time = System.currentTimeMillis();
        String direction = Contract.Log.INCOMING;

        contentValues.put(Contract.Log.COLUMN_NAME_DIRECTION,direction);
        contentValues.put(Contract.Log.COLUMN_NAME_NUMBER,number);
        contentValues.put(Contract.Log.COLUMN_NAME_TIME,time);
        contentValues.put(Contract.Log.COLUMN_NAME_NAME,name);
        contentValues.put(Contract.Log.COLUMN_NAME_COUNTRY_CODE,countryCode);

        SQLiteDatabase db =getWritableDatabase();
        db.insert(Contract.Log.TABLE_NAME, null, contentValues);
        db.close();

    }
    public ArrayList<Contact> getContactList() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Contact> list = new ArrayList<Contact>();
        try {

            String Query = "SELECT  * FROM " + Contract.Log.TABLE_Contact;
            Cursor curContact = db.rawQuery(Query, null);
            if (curContact != null) {
                while (curContact.moveToNext()) {
                    Contact country = new Contact();
                    country.setContactId(curContact.getString(1));

                    country.setCountryCode(curContact.getString(4));


                    country.setName(curContact.getString(2));
                    ;
                    country.setNormalizedNumber(curContact.getString(3));
                    country.setPhoneNumber(curContact.getString(5));


                    list.add(country);

                }
            }

            curContact.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return list;

    }


    public void addContact( String name, String _id ,String nr_numbaer,String Countrycode,String number ) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_NAME, name);
            values.put(KEY_LID, _id);
            values.put(KEY_NR_NUMBER, number);
            values.put(KEY_COUNTRYCODE, Countrycode);
            values.put(KEY_NUMBER, nr_numbaer);

            db.insert(Contract.Log.TABLE_Contact, null, values);
            db.close(); // Closing database connection
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //check table is empty on not
    public boolean  isMasterEmpty( String table) {
        boolean flag;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM"+ table;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int mcount= mcursor.getInt(0);
        flag = mcount <= 0;
        mcursor.close();
        db.close();

        return flag;
    }
    public boolean exists(String table) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count1 = "SELECT * FROM " + table;
            db.rawQuery(count1, null);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
