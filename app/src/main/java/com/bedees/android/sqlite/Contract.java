package com.bedees.android.sqlite;

import android.provider.BaseColumns;

/**
 * Created by MOHIT on 03-01-2016.
 */
public final class Contract {
    public Contract(){

    }

    public static abstract class Log implements BaseColumns {
        public static final String TABLE_NAME = "log";
        public static final String  TABLE_Contact="contactinbox";
        public static final String COLUMN_NAME_DIRECTION = "direction";
        public static final String COLUMN_NAME_NUMBER = "number";
        public static final String COLUMN_NAME_TIME= "time";
        public static final String COLUMN_NAME_NAME= "name";
        public static final String COLUMN_NAME_COUNTRY_CODE= "country_code";

        public static final String INCOMING ="I";
        public static final String OUTGOING ="O";

    }

}
