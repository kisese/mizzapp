package com.bedees.android.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.bedees.android.model.ProfilePicture;
import com.bedees.android.util.FirebaseConstants;
import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

/**
 * Created by vitaliy.herasymchuk on 7/8/16.
 */
public class FirebaseManager {

    public static final String TAG = "FirebaseManager";


    private static final String PROFILE_PICTURES = "profile_pictures";
    public static final String PHONE_NUMBER_FIELD = "phone_no";
    public static final String JPEG = ".jpg";

    private Firebase firebaseImagesRef;
    private FirebaseStorage storage;

    private boolean debug = true;


    private static FirebaseManager instance = new FirebaseManager();
    private Firebase firebaseRef = null;

    public static FirebaseManager getInstance() {
        if (instance == null) {
            instance = new FirebaseManager();
        }
        return instance;
    }

    public FirebaseManager() {
        firebaseRef = new Firebase(FirebaseConstants.FIRE_BASE_URL);
        firebaseImagesRef = new Firebase(FirebaseConstants.FIRE_BASE_URL + "/" + PROFILE_PICTURES + "/");
        storage = FirebaseStorage.getInstance();
    }

    public void uploadNewProfilePicture(Bitmap bitmap, final String phoneNumber, final OnImageUploadedListener listener) {
        try {
            if (bitmap != null && !TextUtils.isEmpty(phoneNumber)) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8; // shrink it down otherwise we will use stupid amounts of memory
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
                byte[] bytes = baos.toByteArray();

                StorageReference storageRef = storage.getReferenceFromUrl(FirebaseConstants.STORAGE_REF);
                String imageTitle = phoneNumber + JPEG;
                StorageReference mountainsRef = storageRef.child(phoneNumber + JPEG);
                StorageReference mountainImagesRef = storageRef.child("images/" + imageTitle);

                mountainsRef.getName().equals(mountainImagesRef.getName());
                mountainsRef.getPath().equals(mountainImagesRef.getPath());

                UploadTask uploadTask = mountainsRef.putBytes(bytes);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        ERROR("uploadNewProfilePicture :: onFailure " + e.toString());
                        listener.onUploadFailed();
                    }
                });

                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        String url;
                        if (taskSnapshot.getDownloadUrl() != null) {
                            url = taskSnapshot.getDownloadUrl().toString();
                            sendImageOnFirebase(url, phoneNumber);
                            listener.onImageUploaded(url);
                        }
                    }
                });

            } else {
                ERROR("uploadNewProfilePicture :: Phone number or picture not valid");
            }

        } catch (Exception e) {
            ERROR(e.toString());
        }
    }


    private void sendImageOnFirebase(String imgUrl, String phoneNumber) {
        if (!TextUtils.isEmpty(imgUrl)) {
            ProfilePicture picture = new ProfilePicture();
            picture.setUrl(imgUrl);
            picture.setPhone_no(phoneNumber);

            Firebase profilePicturesRef = firebaseRef.child(PROFILE_PICTURES);
            profilePicturesRef.child(phoneNumber).setValue(picture);
        }
    }

    public void getProfilePictureForPhoneNumber(String phNum, ValueEventListener listener) {
        Query query = firebaseImagesRef.child(phNum);
        query.addListenerForSingleValueEvent(listener);
    }


    public interface OnImageUploadedListener {
        void onImageUploaded(String url);

        void onUploadFailed();
    }


    private void ERROR(String msg) {
        if (debug) {
            Log.e(TAG, msg);
        }
    }

    private void COMPLAIN(String msg) {
        if (debug) {
            Log.d(TAG, msg);
        }
    }


}
