package com.bedees.android.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bedees.android.R;
import com.bedees.android.item.CatFact;
import com.bedees.android.item.CricketItem;
import com.bedees.android.ui.CatFactFragment;
import com.bedees.android.ui.CricketMatchFragment;
import com.bedees.android.util.SavePref;

import java.util.ArrayList;

/**
 * Created by mk on 4/24/2016.
 */
public class CatFactAdapter extends BaseAdapter {

    private int i = 0;
    private FragmentActivity activity;
    private ArrayList<CatFact> list;
    private SavePref pref;
    private CatFactFragment cricketMatchFragment;

    public CatFactAdapter(FragmentActivity activity, ArrayList<CatFact> cricket_list, CatFactFragment cricketMatchFragment) {

        list = cricket_list;
        this.activity = activity;
        this.cricketMatchFragment = cricketMatchFragment;
        pref = new SavePref(this.activity);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_cricketmatch, null);

            holder.lb_match = (TextView) convertView.findViewById(R.id.lb_match);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.lb_match.setText(list.get(position).getSucess());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((CatFactFragment) cricketMatchFragment).MatchScoreApi(list.get(position).getSucess());

            }
        });

        return convertView;
    }


    class ViewHolder {
        TextView lb_match;

    }


}