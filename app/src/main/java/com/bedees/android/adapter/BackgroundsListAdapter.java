package com.bedees.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bedees.android.R;

import java.util.List;

/**
 * Created by Brayo on 8/18/2016.
 */
public class BackgroundsListAdapter extends ArrayAdapter<String> {

    private final LayoutInflater inflater;
    private Context context;
    private List<String> letters;
    private int[] background_pics = {R.drawable.ic_none, R.drawable.ic_birthday, R.drawable.ic_easy, R.drawable.ic_guitar,
    R.drawable.ic_happy, R.drawable.ic_instrumental, R.drawable.ic_lullaby, R.drawable.ic_piano,
            R.drawable.ic_rain, R.drawable.ic_rythmic, R.drawable.ic_winter, R.drawable.ic_crowd,
    R.drawable.laughter_ic, R.drawable.yabba_dabba, R.drawable.ic_dj, R.drawable.ic_cashier,
    R.drawable.ic_temple, R.drawable.ic_story, R.drawable.ic_story, R.drawable.ic_princess, R.drawable.ic_electro};

    public BackgroundsListAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        letters = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.effect_item, null);
            holder.effect_pic = (ImageView) view.findViewById(R.id.effect_pic);
            holder.effect_name = (TextView) view.findViewById(R.id.effect_name);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.effect_pic.setImageDrawable(context.getResources().getDrawable(background_pics[position]));
        holder.effect_name.setText(letters.get(position));

        return view;
    }

    public class ViewHolder {
        private ImageView effect_pic;
        private TextView effect_name;
    }
}
