package com.bedees.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bedees.android.R;

import java.util.List;

/**
 * Created by Brayo on 8/13/2016.
 */
public class EffectsListAdapter extends ArrayAdapter<String> {

    private final LayoutInflater inflater;
    private Context context;
    private List<String> letters;
    private int[] effect_pics = {R.drawable.ic_none, R.drawable.ic_flanger, R.drawable.ic_roll, R.drawable.ic_whoosh,
                    R.drawable.ic_echo, R.drawable.ic_reverb, R.drawable.ic_limiter, R.drawable.ic_gate,
                    R.drawable.ic_compress, R.drawable.ic_band_eq};

    public EffectsListAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        letters = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.effect_item, null);
            holder.effect_pic = (ImageView) view.findViewById(R.id.effect_pic);
            holder.effect_name = (TextView) view.findViewById(R.id.effect_name);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.effect_name.setText(letters.get(position));
        holder.effect_pic.setImageDrawable(context.getResources().getDrawable(effect_pics[position]));

        return view;
    }

    public class ViewHolder {
        private ImageView effect_pic;
        private TextView effect_name;
    }
}
