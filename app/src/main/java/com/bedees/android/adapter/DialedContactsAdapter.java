package com.bedees.android.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bedees.android.R;
import com.bedees.android.listeners.CommonClickListener;
import com.bedees.android.listeners.OnProfilePopupListener;
import com.bedees.android.manager.FirebaseManager;
import com.bedees.android.model.Contact;
import com.bedees.android.model.ProfilePicture;
import com.bedees.android.util.Utilities;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.List;


/**
 * Created by MOHIT on 18-12-2015.
 */
public class DialedContactsAdapter extends RecyclerView.Adapter<DialedContactsAdapter.ViewHolder> {

    public static final String TAG = DialedContactsAdapter.class.getSimpleName();
    private OnProfilePopupListener mOnProfilePopupListener = null;

    // declare the builder object once.
    TextDrawable.IBuilder builder = TextDrawable.builder()
            .beginConfig()
            .width(72)
            .height(72)
            .withBorder(1)
            .endConfig()
            .round();

    private ColorGenerator generator = ColorGenerator.MATERIAL;

    private List<Contact> mDataSet;
    private CommonClickListener listener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View view;
        public TextView nameTextView, timeTextView;
        public ImageView circularNetworkImageView;
        public int position;
        private Contact contact;

        public ViewHolder(View v, final CommonClickListener listener) {
            super(v);
            view = v;
            nameTextView = (TextView) v.findViewById(R.id.tv_name);
            timeTextView = (TextView) v.findViewById(R.id.tv_time);
            circularNetworkImageView = (ImageView) v.findViewById(R.id.civ_image);
            position = 0;


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCommonClick(v, getAdapterPosition());
                }
            });

            nameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCommonClick(v, getAdapterPosition());
                }
            });

            circularNetworkImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //listener.onCommonClick(v, getAdapterPosition());
                    notifyShowPopup(contact);

                }
            });
        }

        public void bindContact(Contact contact) {
            this.contact = contact;
        }

    }


    public DialedContactsAdapter(List<Contact> items, CommonClickListener listener) {
        this.mDataSet = items;
        this.listener = listener;

    }

    public Contact getItem(int position) {
        if (mDataSet != null && mDataSet.size() > position) {
            return mDataSet.get(position);
        }
        return null;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recent_contact_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v, listener);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Contact contact = mDataSet.get(position);
        holder.bindContact(contact);
        holder.nameTextView.setText(contact.getName());
        holder.timeTextView.setText(contact.getFormattedTime());
        Bitmap bitmap = BitmapFactory.decodeResource(holder.view.getResources(), R.mipmap.ic_def_contact);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(holder.view.getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        roundedBitmapDrawable.setCornerRadius(Math.max(bitmap.getWidth(), bitmap.getHeight()) / 2.0f);

        String phone = contact.getPhoneNumber();
        showContactPicture(holder, phone, contact);
        //holder.circularNetworkImageView.setImageDrawable(roundedBitmapDrawable);

        //TODO set the image view appropriately
        //holder.circularImageView.set
        holder.position = position;


    }

    private void showContactPicture(final DialedContactsAdapter.ViewHolder holder,
                                    String contactPhoneNumber, final Contact contact) {

        FirebaseManager.getInstance().getProfilePictureForPhoneNumber(contactPhoneNumber, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    ProfilePicture picture = dataSnapshot.getValue(ProfilePicture.class);
                    if (picture != null) {
                        contact.setImageUrl(picture.getUrl());
                        Glide.with(holder.view.getContext()).load(picture.getUrl()).asBitmap()
                                .centerCrop().into(new BitmapImageViewTarget(holder.circularNetworkImageView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(holder.itemView.getContext().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                holder.circularNetworkImageView.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } else {
                        holder.circularNetworkImageView.setImageDrawable(getDefaultBitmap(contact));
                    }
                } catch (Exception e) {
                    holder.circularNetworkImageView.setImageDrawable(getDefaultBitmap(contact));
                    Log.e(TAG, e.toString());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                holder.circularNetworkImageView.setImageDrawable(getDefaultBitmap(contact));
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    private Drawable getDefaultBitmap(Contact contact) {
        int color = generator.getColor(contact.getName());
        // reuse the builder specs to create multiple drawables
        TextDrawable ic1 = builder.build("" + contact.getName().charAt(0), color);
        return ic1;
    }

    private void notifyShowPopup(Contact contact) {
        if (mOnProfilePopupListener != null) {
            mOnProfilePopupListener.onShowProfilePopup(contact);
        }
    }

    public void setOnProfilePopupListener(OnProfilePopupListener mOnProfilePopupListener) {
        this.mOnProfilePopupListener = mOnProfilePopupListener;
    }
}


